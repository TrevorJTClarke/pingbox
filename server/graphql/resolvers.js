import settings from './resolvers/settings'
import system from './resolvers/system'

const resolvers = {
  Query: {
    ...(settings.Query ? settings.Query : null),
    ...(system.Query ? system.Query : null),
  },

  Mutation: {
    ...(settings.Mutation ? settings.Mutation : null),
    ...(system.Mutation ? system.Mutation : null),
  },

  Subscription: {
    ...(settings.Subscription ? settings.Subscription : null),
    ...(system.Subscription ? system.Subscription : null),
  },
}

export default resolvers
