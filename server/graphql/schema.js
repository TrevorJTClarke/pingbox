import { gql } from 'apollo-server-express'
import settings from './schemas/settings'
import system from './schemas/system'

const typeDefs = gql`
  ${settings.Single}
  ${system.Single}

  type Query {
    ${settings.Query}
    ${system.Query}
  }

  type Mutation {
    ${settings.Mutation}
    ${system.Mutation}
  }

  type Subscription {
    ${settings.Subscription}
    ${system.Subscription}
  }
`

export default typeDefs
