export default {
  Single: `
    type AlertConfig {
      uuid: String
      active: Boolean
      app_id: String
      network: String
      type: String
      title: String
      desc: String
      category: String
      account_ids: [String]
      email: Boolean
      inbox: Boolean
      rocketchat: Boolean
      slack: Boolean
      sms: Boolean
    }

    type UserSettings {
      user_id: String
      user_referral_id: String
      sub_account_ids: [String]
      subscription_contract_id: String
      timezone: String
      currency: String

      email: String
      sms: String

      productInfoActive: Boolean
      rocketChatActive: Boolean
      rocketChatChannel: String
      rocketChatDomain: String
      rocketChatToken: String
      slackActive: Boolean
      slackChannel: String
      slackToken: String

      alerts: [AlertConfig]
    }

    type UserTransactions {
      hash: String
      amount: String
      status: String
      type: String
      timestamp: String
      title: String
    }

    type UserRewards {
      account_id: String
      amount: String
    }

    type Test {
      type: String
    }
  `,

  Query: `
    userSettings(user_id: String): UserSettings

    rewards(user_id: String): [UserRewards]

    transactions(user_id: String): [UserTransactions]
  `,

  Mutation: `
    createUser(
      user_id: String
      account_id: String
      data_hash: String
      email: String
      sms: String
      timezone: String
      currency: String
    ): UserSettings

    verifyUser(
      user_id: String
      user_referral_id: String
      subscription_contract_id: String
      verify_code: String
    ): UserSettings

    updateUserSettings(
      user_id: String
      sub_account_ids: [String]
      subscription_contract_id: String
      user_referral_id: String
      email: String
      sms: String
      timezone: String
      currency: String
      productInfoActive: Boolean
      rocketChatActive: Boolean
      rocketChatChannel: String
      rocketChatDomain: String
      rocketChatToken: String
      slackActive: Boolean
      slackChannel: String
      slackToken: String
    ): UserSettings

    updateUserAlert(
      uuid: String
      user_id: String
      app_id: String
      network: String
      type: String
      sub_account_ids: [String]
      active: Boolean
      email: Boolean
      inbox: Boolean
      rocketchat: Boolean
      slack: Boolean
      sms: Boolean
    ): AlertConfig

    sendUserAlertTest(
      user_id: String
      type: String
    ): Test

    sendVerifyEmail(
      user_id: String
      email: String
    ): Test
  `,

  Subscription: `
    updateUserSettings: [UserSettings]
  `
}
