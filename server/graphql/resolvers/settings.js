import db from '../../db/index'
import { populateAlertDetails, dappSettings } from '../../dapps'
import { activeDapps } from '../../dapps/pingbox'
import { getNetworkFromContractId, baseServerUrl } from '../../helpers/utils'
import EmailProvider from '../../providers/email'
import Messages from '../../providers/messages'
import { sendVerifyEmail } from '../../providers/subscriptions'
import { getMd5 } from '../../db/encrypter'

const messenger = new Messages()

const fillDefaults = data => {
  // Fill missing defaults
  if (typeof data.productInfoActive === 'undefined') data.productInfoActive = true
  if (typeof data.rocketChatActive === 'undefined') data.rocketChatActive = false
  if (typeof data.rocketChatChannel === 'undefined') data.rocketChatChannel = 'general'
  if (typeof data.rocketChatDomain === 'undefined') data.rocketChatDomain = ''
  if (typeof data.rocketChatToken === 'undefined') data.rocketChatToken = ''
  if (typeof data.slackActive === 'undefined') data.slackActive = false
  if (typeof data.slackChannel === 'undefined') data.slackChannel = 'general'
  if (typeof data.slackToken === 'undefined') data.slackToken = ''
  if (typeof data.alerts === 'undefined') data.alerts = []

  return data
}

const fillAlertDefaults = data => {
  // Fill missing defaults
  if (typeof data.active === 'undefined') data.active = false
  if (typeof data.type === 'undefined') data.type = 'default'
  if (typeof data.network === 'undefined') data.network = 'testnet'
  if (typeof data.app_id === 'undefined') data.app_id = 'pingbox.testnet'
  if (typeof data.email === 'undefined') data.email = false
  if (typeof data.inbox === 'undefined') data.inbox = false
  if (typeof data.rocketchat === 'undefined') data.rocketchat = false
  if (typeof data.slack === 'undefined') data.slack = false
  if (typeof data.sms === 'undefined') data.sms = false

  return data
}

export default {
  Query: {
    async userSettings(root, args) {
      if (!args || !args.user_id) throw 'Arguments missing!'
      const user_id = getMd5(args.user_id)
      let user
      let alerts

      try {
        const raw = await db.getItemById('users', { user_id })
        user = raw[0]
      } catch (e) {
        throw e
      }
      if (!user) return

      try {
        alerts = await db.getItemById('users_alerts_configs', { user_id })
      } catch (e) {
        // throw e
      }
      if (alerts) user.alerts = alerts

      const network = getNetworkFromContractId(user.account_id)

      // Add UI title, descriptions
      if (user.alerts && user.alerts.length > 0) {
        user.alerts = user.alerts.map(a => {
          return fillAlertDefaults(populateAlertDetails(a))
        })
      }

      // Add non-configured defaults
      Object.keys(dappSettings).forEach(net => {
        const networkSettings = dappSettings[net]
        
        if (network === net) {
          Object.keys(networkSettings).forEach(app_id => {
            const appSettings = dappSettings[net][app_id]
            Object.keys(appSettings.alertTypes).forEach(type => {
              let has = false
              user.alerts.forEach(ua => {
                if (
                  ua.network === network &&
                  ua.app_id === app_id &&
                  ua.type === type
                ) has = true
              })
              if (!has) user.alerts.push(
                fillAlertDefaults(populateAlertDetails({ type, network, app_id }))
              )
            })
          })
        }
      })

      return fillDefaults(user || {})
    },

    async rewards(root, args) {
      if (!args || !args.user_id) throw 'Arguments missing!'
      // return [
      //   { account_id: 'person.testnet', amount: '1' },
      // ]
      const user_id = getMd5(args.user_id)

      try {
        const res = await db.getItemById('users_rewards', { user_id })
        return res
      } catch (e) {
        // throw e
        return []
      }
    },

    async transactions(root, args) {
      if (!args || !args.user_id) throw 'Arguments missing!'
      // return [
      //   {
      //     hash: 'D3VDXNTSzdsRkmwjZnTzr9Aj6PcRGfuK5LBSbMPJv51v',
      //     amount: '12',
      //     status: 'Success',
      //     type: 'payment',
      //     timestamp: 'Jan 1st. 2021',
      //     title: 'Payed',
      //   },
      //   {
      //     hash: 'D3VDXNTSzdsRkmwjZnTzr9Aj6PcRGfuK5LBSbMPJv52v',
      //     amount: '1',
      //     status: 'Success',
      //     type: 'reward',
      //     timestamp: 'Jan 1st. 2021',
      //     title: 'Reward Earned',
      //   },
      // ].reverse()
      const user_id = getMd5(args.user_id)
      console.log('user_id', user_id);

      try {
        const res = await db.getItemById('users_transactions', { user_id })
        console.log('txns', res);
        return res
      } catch (e) {
        // throw e
        return []
      }
    },
  },

  Mutation: {
    createUser: async (root, data) => {
      if (!data || !data.data_hash || !data.email || !data.user_id) throw 'Arguments missing!'
      const item = { ...fillDefaults(data) }
      // NOTE: MD5 handled automatically here
      item.user_id = data.user_id
      item.account_id = data.user_id
      item.data_hash = data.data_hash
      item.email = data.email
      delete item.alerts

      // NOTE: Verify code:
      // Gets stored in DB, then later used after RPC picks up completed TXN

      try {
        await db.insertItem('users', item)
      } catch (e) {
        throw e
      }

      return item
    },
    verifyUser: async (root, data) => {
      if (!data || !data.user_referral_id || !data.verify_code || !data.user_id) throw 'Arguments missing!'
      const user_id = getMd5(data.user_id)
      let user

      try {
        const raw = await db.getItemById('users', { user_id })
        user = raw[0]
        if (!user.verifyToken || user.verifyToken !== data.verify_code) throw 'Invalid verification'
      } catch (e) {
        throw e
      }

      const item = {}
      Object.keys(user).forEach(u => {
        if (typeof user[u] !== 'undefined' && user[u] !== null && user[u] !== '') item[u] = user[u]
      })

      item.user_id = data.user_id
      item.account_id = data.user_id
      item.user_referral_id = data.user_referral_id
      if (data.subscription_contract_id)
        item.subscription_contract_id = data.subscription_contract_id
      item.verifyToken = '-'
      
      try {
        await db.insertItem('users', item)
      } catch (e) {
        throw e
      }

      // Send verify email!
      const p = new EmailProvider()
      const emailData = {
        type: 'Welcome',
        theme: 'dark',
        to: item.user_id,
        ctaTitle: '',
        ctaUrl: ``,
        referralUrl: `${baseServerUrl(data.user_id)}/onboarding?referral=${data.user_referral_id}`,
      }

      try {
        return p.send(emailData, item)
      } catch (e) {
        console.log('sendEmail', e)
      }

      return item
    },
    updateUserSettings: async (root, data) => {
      if (!data || !data.user_id) throw 'Arguments missing!'
      // const item = { ...fillDefaults(data), ...data }
      const item = { ...data }
      item.user_id = data.user_id
      item.account_id = data.user_id
      delete item.alerts

      try {
        await db.insertItem('users', item)
      } catch (e) {
        throw e
      }

      return item
    },
    updateUserAlert: async (root, data) => {
      if (!data || !data.user_id || !data.type) throw 'Arguments missing!'
      const item = { ...fillAlertDefaults(data) }
      if (!item.uuid) item.uuid = getMd5(`${+new Date()}${data.user_id}${data.type}`)

      try {
        await db.insertItem('users_alerts_configs', item, 'uuid')
      } catch (e) {
        throw e
      }

      return item
    },
    sendVerifyEmail: async (root, data) => {
      return sendVerifyEmail(data)
    },
    sendUserAlertTest: async (root, data) => {
      if (!data || !data.user_id) throw 'Arguments missing!'
      const user_id = getMd5(data.user_id)
      const type = `${data.type}`

      // Get user data:
      let user
      try {
        const raw = await db.getItemById('users', { user_id })
        user = raw[0]
      } catch(e) {
        throw 'No user found!'
      }
      if (!user) return
      // Send email based on type, add fake data
      const settings = { ...user }
      const app_id = data.user_id.search('\.testnet') > -1 ? activeDapps[0] : activeDapps[1]
      settings.referralId = user.user_referral_id
      settings.recipient = data.user_id
      settings.alerts = [{ active: true, type: 'transfer', app_id, email: true }]
      data.type = 'transfer'
      data.txHash = 'G4451kRsRvNUkvGhMumvox1bjiihfqB5rW8b9LUqTViB'
      data.blockHash = 'HEYvg5TRtospSx2YNan7VTkC5Y2k4trb8YoZ679A21RG'
      data.from = 'pingbox.testnet'
      data.to = data.user_id
      data.fee = '22318256250000000000'
      data.value = '1.002'
      data.valueUSD = '1.232460'
      data.feeUSD = '0.000027'

      // TODO: Upgrade to latest payload needs
      const payloads = messenger.getFormattedMessage(data.type, data, settings, false)

      if (type === 'email') {
        try {
          return messenger.sendEmail(payloads.email, user)
        } catch (e) {
          console.log('sendEmail', e)
        }
      }
      if (type === 'slack') {
        try {
          return messenger.sendSlack(payloads.slack, user)
        } catch (e) {
          console.log('sendSlack', e)
        }
      }
      if (type === 'rocketchat') {
        try {
          return messenger.sendRocketchat(payloads.rocketchat, user)
        } catch (e) {
          console.log('sendRocketchat', e)
        }
      }

      return { type: 'done' }
    },
  },

  Subscription: {
    updateUserSettings: {
      subscribe: () => pubsub.asyncIterator(EVENTS.USER_SETTINGS_UPDATED),
    },
  },
}
