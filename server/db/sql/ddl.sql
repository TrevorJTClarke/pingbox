-- DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users
(
   "user_id"                      VARCHAR(256)                NOT NULL
  ,"account_id"                   VARCHAR(256)                NOT NULL
  ,"sub_account_ids"     		      VARCHAR(256)
  ,"subscription_contract_id"     VARCHAR(256)
  ,"user_referral_id"             VARCHAR(256)
  ,"data_hash"                    VARCHAR(256)
  ,"verifyToken"                  VARCHAR(256)
  ,"timezone"                     VARCHAR(256)
  ,"currency"                     VARCHAR(256)
  ,"email"                        VARCHAR(256)
  ,"sms"                          VARCHAR(256)
  ,"productInfoActive"            BOOLEAN                     NOT NULL DEFAULT TRUE
  ,"rocketChatActive"             BOOLEAN                     NOT NULL DEFAULT FALSE
  ,"rocketChatChannel"            VARCHAR(256)
  ,"rocketChatDomain"             VARCHAR(256)
  ,"rocketChatToken"              VARCHAR(256)
  ,"slackActive"                  BOOLEAN                     NOT NULL DEFAULT FALSE
  ,"slackChannel"                 VARCHAR(256)
  ,"slackToken"                   VARCHAR(256)
  ,"createdAt"                    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()

  ,PRIMARY KEY ("user_id")
);

-- ------------------------------------------------------------------------

-- DROP TABLE IF EXISTS users_alerts_configs;
CREATE TABLE IF NOT EXISTS users_alerts_configs
(
   "uuid"           VARCHAR(256)                NOT NULL
  ,"user_id"        VARCHAR(256)                NOT NULL
  ,"active"         BOOLEAN                     NOT NULL
  ,"app_id"         VARCHAR(256)                NOT NULL
  ,"network"        VARCHAR(256)                NOT NULL
  ,"type"           VARCHAR(256)                NOT NULL
  ,"title"          VARCHAR(256)
  ,"category"       VARCHAR(256)
  ,"desc"           VARCHAR
  ,"account_ids"    VARCHAR
  ,"email"          BOOLEAN                     NOT NULL DEFAULT TRUE
  ,"inbox"          BOOLEAN                     NOT NULL DEFAULT TRUE
  ,"rocketchat"     BOOLEAN                     NOT NULL DEFAULT FALSE
  ,"slack"          BOOLEAN                     NOT NULL DEFAULT FALSE
  ,"sms"            BOOLEAN                     NOT NULL DEFAULT FALSE

  ,PRIMARY KEY ("uuid")
);

-- ------------------------------------------------------------------------

-- DROP TABLE IF EXISTS users_alerts;
CREATE TABLE IF NOT EXISTS users_alerts
(
   "timestamp"      TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
  ,"uuid"           VARCHAR(256)                NOT NULL
  ,"user_id"        VARCHAR(256)                NOT NULL
  ,"type"           VARCHAR                     NOT NULL
  ,"name"           VARCHAR
  ,"subscription"   VARCHAR
  ,"data"           VARCHAR

  ,PRIMARY KEY ("timestamp")
);

-- ------------------------------------------------------------------------

-- DROP TABLE IF EXISTS users_transactions;
CREATE TABLE IF NOT EXISTS users_transactions
(
   "user_id"        VARCHAR(256)                NOT NULL
  ,"timestamp"      TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
  ,"hash"           VARCHAR(256)                NOT NULL
  ,"type"           VARCHAR                     NOT NULL
  ,"amount"         VARCHAR
  ,"title"          VARCHAR
  ,"status"         VARCHAR

  ,PRIMARY KEY ("user_id")
);

-- ------------------------------------------------------------------------

-- DROP TABLE IF EXISTS users_rewards;
CREATE TABLE IF NOT EXISTS users_rewards
(
   "uuid"           VARCHAR(256)                NOT NULL
  ,"user_id"        VARCHAR(256)                NOT NULL
  ,"account_id"     VARCHAR(256)                NOT NULL
  ,"amount"         VARCHAR                     NOT NULL

  ,PRIMARY KEY ("uuid")
);

-- ------------------------------------------------------------------------

-- DROP TABLE IF EXISTS subscriptions;
CREATE TABLE IF NOT EXISTS subscriptions
(
   "user_id"        VARCHAR(256)                NOT NULL
  ,"timestamp"      TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
  ,"hash"           VARCHAR(256)                NOT NULL
  ,"period"         VARCHAR                     NOT NULL
  ,"active"         BOOLEAN                     NOT NULL DEFAULT TRUE
  ,"contract_id"    VARCHAR(256)
  ,"status"         VARCHAR
  ,"amount"         VARCHAR
  ,"title"          VARCHAR
  ,"desc"           VARCHAR

  ,PRIMARY KEY ("user_id")
);

-- ------------------------------------------------------------------------

-- DROP TABLE IF EXISTS analytics;
CREATE TABLE IF NOT EXISTS analytics
(
   "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
  ,"type"           VARCHAR(256)                NOT NULL
  ,"event"          VARCHAR                     NOT NULL
  ,"category"       VARCHAR
  ,"action"         VARCHAR
  ,"data"           VARCHAR

  ,PRIMARY KEY ("timestamp")
);