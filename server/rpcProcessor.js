require('dotenv').config()
import QueueProcessor from './queueProcessor'
import NearProvider from './providers/near'
import * as cache from './helpers/cache'
import { getRecipients } from './dapps'
import Formatters from './helpers/formatters'
import { getRootAccountIds, rootContainsDapp } from './helpers/utils'
import Messages from './providers/messages'
import { getPrice } from './providers/coingecko'
import { getUserSettings, setSubscription, sendVerifyEmail, sendReferralRewardMessage } from './providers/subscriptions'

const BLOCK_LAG_NUMBER = 20

const near = new NearProvider()
const format = new Formatters()
const messager = new Messages()

// in memory quick cache -- could be moved if necessary later
let allSubscriptionIds = []
const subscriptions = {}
const prices = { near: {} }

export default class RpcProcessor {
  constructor() {
    this.q = new QueueProcessor({
      block_height: this.processBlockHeight.bind(this),
      transactions: this.processTransactions.bind(this),
      messages: this.processMessages.bind(this),
      cron: this.processCron.bind(this),
    })

    // simple caches
    this.blockHeightCache = []

    return this
  }

  async init() {
    await near.init()
    await this.initContract()
    if (process.env.NEAR_QUEUE_PROCESSING == 'true') this.startQueueProcessors()
    if (process.env.NEAR_RPC_POLLING == 'true') this.pollBlockHeight()
  }

  // poll block height, populate block height queue
  // Then push blocks to process into queue based on subtraction to last known block height
  pollBlockHeight() {
    near.pollBlocks(data => {
      const block = parseInt(data.id) - BLOCK_LAG_NUMBER
      const maxKnown = this.blockHeightCache.length > 0 ? Math.max(...this.blockHeightCache) : block
      const offset = block - maxKnown
      let i = 0
      // console.log('blocks:', maxKnown, block)
      console.log('BLOCK HEIGHT', block, new Date().toISOString());

      while (i <= offset) {
        let id = maxKnown + i
        // Check if this block height has been processed before adding to queue
        if (!this.blockHeightCache.includes(id)) {
          this.blockHeightCache.push(id)
          // console.log('add block height:', id);
          this.q.add('block_height', { id })
        }
        i++
      }
    })
  }

  // listen to block height job queue and get all txns data, populate txn queue
  startQueueProcessors() {
    console.log('⍈ QUEUE PROCESSOR: Started Ongoing & Cron')
    this.q.run()

    // Register all cron schedules
    this.q.schedule('price', {}, '0/5 * * * *') // every 5 minutes

    // Start immediate actions, so caches can be full!
    this.processCron()
  }

  // process for txns, then add to txns
  async processBlockHeight(data, callback) {
    if (!data || !data.id) throw 'Block id or hash required'
    let block
    try {
      block = await near.getBlockData(data.hash || data.id)
    } catch (e) {
      console.log('processBlockHeight block', e)
    }

    const transactions = []
    try {
      const txns = await near.getTransactionsDataFromBlock(block)
      if (txns && txns.length > 0) {
        txns.forEach(t => {
          const transaction = format.transaction(t, { prices })
          // NOTE: Maybe make a setting here for debug?
          if (
            transaction &&
            this.filterTxns(transaction)
          ) transactions.push(transaction)
        })
      }
      if (typeof callback === 'function') callback(transactions)
    } catch (e) {
      console.log('processBlockHeight txns', e)
    }
  }

  // process for messages by getting subscription info, then add to messages queue
  async processTransactions(data, callback) {
    const messages = []

    // Watch main dapp contract for log events, 
    // TODO: Change to active dapps?
    if (data.contract === near.config.contractNames.pingbox) {
      const logs = this.getCoreLogEvents(data.logs)
      if (logs) await this.processPingboxLogs(logs, data)
      await this.processPingboxMethods(data)
    }
    // If txn involving a subscription, parse for message (using txn queue)
    else {
      if (allSubscriptionIds.includes(data.to)) {
        const subscription = await this.getSubscriptionByAccountId(data.to)
        if (subscription) messages.push({ account_id: data.to, subscription, data })
      }
      if (allSubscriptionIds.includes(data.from)) {
        const subscription = await this.getSubscriptionByAccountId(data.from)
        if (subscription) messages.push({ account_id: data.from, subscription, data })
      }

      // Testing only
      if (process.env.DUMMY_RPC_POLLING === 'true') {
        messages.push({ account_id: data.to, subscription: {}, data })
      }
    }

    if (typeof callback === 'function') callback(messages)
  }

  // add final outcome to sent messages DB table
  async processMessages(message) {
    // Get known dapp accountIds
    const activeDapps = cache.get('activeDapps')
    const rootIds = getRootAccountIds(message.data)
    const isDappMessage = rootContainsDapp(rootIds.all, activeDapps)
    const rootDapp = rootIds.to.replace('\.testnet', '').replace('\.near', '')
    let recipients = []

    // Assess message.type for common changes
    message.type = format.normalizeType(message.data.type)

    // Get dapp recipient list, allowing custom logic per-dapp if necessary
    // In the future extend this to work per-dapp configuration via log or data receipt
    if (isDappMessage) {
      try {
        const res = await getRecipients(rootDapp, message.data.to, message.data.from, message.data)
        if (res && res.length > 0) recipients = recipients.concat(res)
      } catch (e) {
        console.log('dapp recipients e', e);
      }
    } else {
      recipients.push(message.account_id || message.data.to)
    }
    console.log('recipients', recipients);

    if (process.env.DUMMY_RPC_POLLING === 'true') {
      // TESTING ONLY!!!!!!!!!!!!!!!!!!
      message.settings = {
        // email: 'fake@email.me',
        email: 'besnoid@gmail.com',
        // rocketChatToken: 'bLQGZZmu2EZvbw9gX/fXay6KApbhPpngR4yP5RSbno6uQMsYLDuN2kEWMebHdefAT9',
        // rocketChatDomain: 'chat.clarke.club',
        // rocketChatChannel: 'hobbyhodlr-tc',
        slackToken: 'T4LSJEX7G/B4LSYAS3Z/2zOcMdVMw5vrRgPKk6BYF77I',
        slackChannel: 'alerts',
      }
      message.data.recipient = recipients[0]
      return messager.send(message, isDappMessage)
    }

    // loop to get settings and format, then send
    await Promise.all(recipients.map(async recipient => {
      message.data.recipient = recipient

      // Get recipient from DB
      let user
      try {
        // TODO: Support sub-account IDs
        user = await getUserSettings({ user_id: recipient })
        message.settings = user
        if (user) message.subscription = await this.getSubscriptionByAccountId(recipient)
      } catch (e) {
        // throw e
        console.log('GET RECIPIENT USER SETTINGS', e);
      }

      if (!message.subscription || !message.subscription.active) return
      if (!message.settings || Object.keys(message.settings).length < 1) return
      return messager.send(message, isDappMessage)
    }))
  }

  // Repeat processes
  async processCron() {
    // get spot prices
    const spot = await getPrice()
    // Set in a cache of prices
    prices.near = spot
  }

  async initContract() {
    if (!near || !near.pingboxContract) throw 'Near manager contract required!'
    const contract = await near.pingboxContract()
    const statistics = await contract.get_stats()
    if (statistics) {
      const stats = {
        version: statistics[0],
        subscription_fee: statistics[1],
        validation_reward: statistics[2],
        referral_reward: statistics[3],
        subscriptions: statistics[4],
        referrals: statistics[5],
      }
      console.log(`📊 Contract Stats:: V:${stats.version}, ID: ${near.config.contractNames.pingbox}, Total Subscribers: ${stats.subscriptions}, Referrals: ${stats.referrals}`)
    }

    // Get all subscription IDs from contract
    const all_subs = await contract.get_all_subscriptions()
    allSubscriptionIds = all_subs.split(',')
    console.log('allSubscriptionIds', allSubscriptionIds);
  }

  // filtering out txns we dont care to process
  filterTxns(txn) {
    // Fast return for failed txns:
    if (!txn.status || txn.status === 'fail') return false
    let hasSub = false

    const rootIds = getRootAccountIds(txn)
    const contractIds = Object.values(near.config.contractNames)

    if (allSubscriptionIds.includes(txn.to) || allSubscriptionIds.includes(txn.from)) hasSub = true
    if (contractIds.includes(txn.contract) || contractIds.includes(txn.to) || contractIds.includes(txn.from)) hasSub = true

    if (rootIds && rootIds.all && rootIds.all.length > 0) {
      rootIds.all.forEach(id => {
        if (txn.to.search(id) > -1 || txn.from.search(id) > -1) hasSub = true
      })
    }
    return hasSub
  }
  
  // return any logs from the core contract
  getCoreLogEvents(logs) {
    const fin = []
    logs.forEach(log => {
      const params = log.split(':')
      if (params.length > 1 && params[0] === 'SUB') {
        fin.push({ type: params[1], args: params[2] })
      }
    })
    return fin
  }

  async processPingboxLogs(logs, txn) {
    if (!logs) return
    let res

    try {
      res = await Promise.all(logs.map(async log => {
        switch (log.type) {
          case 'REF':
            // Trigger referral reward earned email
            console.log('sendReferralRewardMessage', log);
            await sendReferralRewardMessage({ user_referral_id: log.args }, txn)
            break;
          default:
            break;
        }
      }))
    } catch (e) {
      console.log('processPingboxLogs', e);
    }

    return res
  }

  async processPingboxMethods(data) {
    if (!data || !data.actions) return
    let res

    try {
      res = await Promise.all(data.actions.map(async action => {
        if (typeof action === 'string') return;
        if (!action || !action.method_name) return
        const account_id = data.from
        const method = action.method_name
        const args = action.args

        // NOTE: Also available on args: args.hash
        const referral_id = args && args.rfr ? args.rfr : null

        switch (method) {
          // add_subscription
          case near.config.abis.pingbox.changeMethods[0]:
            // add account ID
            allSubscriptionIds.push(account_id)
            // get subscription data, add to cache, establish DB data
            await this.getSubscriptionByAccountId(account_id)
            await this.storeNewSubscription(account_id, data, referral_id)
            await sendVerifyEmail({ user_id: account_id })
            break;

          // validate_subscription
          case near.config.abis.pingbox.changeMethods[1]:
            // Let graphql onboarding flow work, instead of handling here.
            break;

          // update_subscription
          case near.config.abis.pingbox.changeMethods[2]:
            // add account ID
            allSubscriptionIds.push(account_id)
            // get subscription data and add to cache
            await this.getSubscriptionByAccountId(account_id)
            break;

          default:
            break;
        }
      }))
    } catch (e) {
      console.log('processPingboxMethods', e);
    }

    return res 
  }

  // get relevent subscription data so we know how to process for user
  async getSubscriptionByAccountId(account_id) {
    if (subscriptions[account_id]) return subscriptions[account_id]
    if (!near || !near.pingboxContract) throw 'Near manager contract required!'
    const contract = await near.pingboxContract()

    try {
      const res = await contract.get_subscription({ id: account_id })
      subscriptions[account_id] = {
        active: res[0],
        verified: res[1],
        subscribe_block: res[2],
        balance: res[3],
        referral_code: res[4],
        account_hash: res[5],
        contract_id: res[6],
        data_hash: res[7],
      }
      if (subscriptions[account_id]) return subscriptions[account_id]
    } catch (e) {
      console.log('getSubscriptionByAccountId', e)
    }
  }

  // get relevent subscription data so we know how to process for user
  storeNewSubscription(account_id, txn, referral_id) {
    if (!subscriptions[account_id]) return
    const data = {
      user_id: txn.from,
      timestamp: new Date().toISOString(),
      hash: txn.txHash,
      amount: txn.value,
      contract_id: subscriptions[account_id].contract_id,
    }
    return setSubscription(data)
  }
}