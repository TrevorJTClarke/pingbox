import Queue from 'bull'

// Responsible for checking every X time, loading pertinent operations & executing them
// REF: https://optimalbits.github.io/bull/
export default class QueueProcessor {
  constructor(processors) {
    // Setup initial queues
    this.q = {}
    this.q.blockHeight = new Queue('block_height')
    this.q.transactions = new Queue('transactions')
    this.q.messages = new Queue('messages')
    this.q.cron = new Queue('cron')
    this.processors = processors

    return this
  }

  run() {
    if (typeof this.processors.block_height === 'function')
      this.q.blockHeight.process((job, done) => {
        if (!job || !job.data) return done()
        // console.log('BLOCK: NEW', job.data.id);
        this.processors.block_height(job.data, items => {
          items.forEach(i => {
            this.add('transactions', i)
          })
        })
        done()
      })
    if (typeof this.processors.transactions === 'function')
      this.q.transactions.process((job, done) => {
        if (!job || !job.data) return done()
        this.processors.transactions(job.data, items => {
          items.forEach(i => {
            this.add('messages', i)
          })
        })
        done()
      })
    if (typeof this.processors.messages === 'function')
      this.q.messages.process((job, done) => {
        if (!job || !job.data) return done()
        this.processors.messages(job.data)
        done()
      })
    if (typeof this.processors.cron === 'function')
      this.q.cron.process((job, done) => {
        if (!job || !job.data) return done()
        this.processors.cron(job.data)
        done()
      })
  }

  async add(type, payload) {
    const data = {
      ...payload,
      name: type,
      removeOnComplete: true,
      // removeOnFail: true,
    }

    switch (type) {
      case 'block_height':
        await this.q.blockHeight.add(data, {})
        break;
      case 'transactions':
        await this.q.transactions.add(data, {})
        break;
      case 'messages':
        await this.q.messages.add(data, {})
        break;
      default:;
    }
  }

  schedule(type, payload, cron) {
    const data = { ...payload, name: type }
    return this.q.cron.add(data, { repeat: { cron }})
  }

  // find job, then remove it
  async removeJob(type, item) {
    let jobs

    switch (type) {
      case 'block_height':
        jobs = this.q.blockHeight.getJobs()
        break;
      case 'transactions':
        jobs = this.q.transactions.getJobs()
        break;
      case 'messages':
        jobs = this.q.messages.getJobs()
        break;
      default: ;
    }
    if (!jobs || jobs.length < 1) return
    await jobs.forEach(async j => {
      if (j.id === item.id) await j.remove()
    })
  }

  // This literally brute force clears EVERYTHING
  async clearAll() {
    this.mainQueue.clean(1000)
    const jobs = await this.mainQueue.getJobs()
    await jobs.forEach(async job => await job.remove())
    return this.mainQueue.empty()
  }
}