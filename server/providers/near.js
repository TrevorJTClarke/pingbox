require('dotenv').config()
import { connect, keyStores, Account, Contract, utils } from 'near-api-js'
import getNearConfig from '../helpers/config'
import * as dummy from '../helpers/testRpcs'

const env = process.env.NODE_ENV || 'development'
const isDummyRpc = process.env.DUMMY_RPC_POLLING === 'true'

export default class NearProvider {
  constructor(options = {}) {
    const MergeKeyStore = keyStores.MergeKeyStore
    const UnencryptedFileSystemKeyStore = keyStores.UnencryptedFileSystemKeyStore
    const stores = [
      // new UnencryptedFileSystemKeyStore(CREDENTIALS_DIR),
      new UnencryptedFileSystemKeyStore(process.env.NEAR_PROJECT_KEY_DIR || './neardev')
    ]
    const keyStore = new MergeKeyStore(stores)
    const nearConfig = getNearConfig(options.networkId || env)
    this.config = {
      keyStore,
      ...nearConfig,
      ...options,
      // the SERVER wallet account id
      account_id: options.account_id || nearConfig.contractNames.pingbox || 'pingbox.testnet',
    }

    // create new near connection based on config
    this.client = null
  }

  async init() {
    const client = await connect(this.config)
    this.client = client.connection
    // if (this.client) console.log('Ⓝ EAR: Connected')
  }

  // Polls for block height to let the system know which block and block header are available
  async pollBlocks(jobCallback) {
    if (!this.client) throw 'No client connected!'
    let running = false
    console.log('🀫 BLOCK POLLING: Started')
    setInterval(async () => {
      if (!running) {
        running = true

        // Helper for testing
        if (isDummyRpc) {
          // get local files and iterate through
          const blockHeight = await dummy.getBlockHeight()
          if (jobCallback) jobCallback(blockHeight)
        } else {
          try {
            const blockHeight = await this.getBlockHeight()
            if (jobCallback) jobCallback(blockHeight)
          } catch (e) {
            // TODO: Throw back for retry
            // console.log('RPC: BLOCK POLLING:', e)
            console.log('RPC: GET BLOCK POLLING ERROR')
            throw e
          }
        }
        running = false
      }
    }, process.env.NEAR_POLL_INTERVAL || 5000)
  }

  // Returns a single block info, based on latest known chain state
  async getBlockHeight() {
    if (!this.client) throw 'No client connected!'
    let chain
    try {
      chain = (await this.client.provider.status()).sync_info
    } catch (e) {
      // console.log('RPC: BLOCK HEIGHT:', e)
      console.log('RPC: GET BLOCK HEIGHT ERROR')
      throw e
    }
    if (!chain || !chain.latest_block_hash) throw 'Could not retrieve latest chain state!'
    return { id: chain.latest_block_height, hash: chain.latest_block_hash }
  }

  // Returns a single block info, based on hash
  async getBlockData(hash) {
    if (isDummyRpc) return dummy.getBlockData()
    if (!this.client) throw 'No client connected!'
    if (!hash) throw 'Block hash required!'
    // get single block data
    try {
      const block = await this.client.provider.block({ blockId: hash })
      return block
    } catch (e) {
      // console.log('RPC: GET BLOCK:', e)
      console.log('RPC: GET BLOCK DATA ERROR:', hash)
      throw e
    }
  }

  // Returns a single chunk info, based on hash
  async getChunkData(hash) {
    if (!this.client) throw 'No client connected!'
    if (!hash) throw 'Chunk hash required!'
    // get single block data
    try {
      const chunk = this.client.provider.chunk(hash)
      return chunk
    } catch (e) {
      console.log('RPC: GET CHUNK:', e)
      throw e
    }
  }

  // Returns a single chunk info, based on hash
  async getTransactionData(hash, signer) {
    if (!this.client) throw 'No client connected!'
    if (!hash) throw 'TX hash required!'
    if (!signer) throw 'TX signer required!'
    // get single block data
    try {
      const decodedTxHash = utils.serialize.base_decode(hash)
      const txn = this.client.provider.txStatus(decodedTxHash, signer)
      return txn
    } catch (e) {
      console.log('RPC: GET TX:', e)
      throw e
    }
  }

  // Returns array of transactions, either from single block/chunk or multiples to make life a teensy bit easier
  async getTransactionsDataFromBlock(block) {
    if (isDummyRpc) return dummy.getTransactionsDataFromBlock()
    if (!this.client) throw 'No client connected!'
    if (!block || !block.chunks || block.chunks.length < 1) return

    // to get a list of transactions processed on a block we have to map the
    // collection of [chunk_hash]es to chunks
    const chunkFromChunkHash = async c => { return await this.getChunkData(c.chunk_hash) }
    let chunksData

    // Get the chunk list of txns
    try {
      chunksData = await Promise.all(block.chunks.map(chunkFromChunkHash))
    } catch (e) {
      console.log('RPC: GET ALL CHUNKS:', e)
      throw e
    }

    if (!chunksData || chunksData.length < 1) return

    let txnPromises = []
    // for each txn hash, queue up a promise
    chunksData.forEach(c => {
      if (c && c.transactions && c.transactions.length) {
        c.transactions.forEach(txn => {
          txnPromises.push(this.getTransactionData(txn.hash, txn.signer_id))
        })
      }
    })

    // process all txn promises
    if (!txnPromises && txnPromises.length < 1) return

    // return raw txns array
    try {
      const txnsData = await Promise.all(txnPromises)
      // console.log(JSON.stringify(txnsData, null, 2))
      return txnsData
    } catch (e) {
      console.log('RPC: GET ALL TXNS:', e)
      throw e
    }
  }

  getAccount(id) {
    return new Account(this.client, id)
  }

  pingboxContract() {
    return new Contract(
      this.getAccount(this.config.account_id), // account,
      this.config.contractNames.pingbox, // contractName,
      this.config.abis.pingbox,
    )
  }

  settingsContract() {
    return new Contract(
      this.getAccount(this.config.account_id), // account,
      this.config.contractNames.settings, // contractName,
      this.config.abis.settings,
    )
  }

  userSettingsContract() {
    return new Contract(
      this.getAccount(this.config.account_id), // account,
      this.config.contractNames.user_settings, // contractName,
      this.config.abis.user_settings,
    )
  }

  async contractInstance(contractId, ABI) {
    await this.init()
    return new Contract(
      this.getAccount(this.config.account_id), // account,
      contractId,
      ABI,
    )
  }
}
