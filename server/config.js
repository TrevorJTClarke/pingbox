export const config = {
  "development": {
    "pingbox": "pingbox.pbx.testnet",
    "settings": "settings.pbx.testnet",
    "user_settings": ".settings.pbx.testnet"
  },
  "production": {
    "pingbox": "manager_v1.pingbox.near",
    "settings": "user_settings_v1.prefs.near",
    "user_settings": ".user_settings_v1.prefs.near"
  },
  "abis": {
    "pingbox": {
      "viewMethods": [
        "get_referral_code",
        "get_all_subscriptions",
        "get_subscription",
        "get_stats",
        "version"
      ],
      "changeMethods": [
        "add_subscription",
        "validate_subscription",
        "update_subscription",
        "update_settings",
        "admin_transfer_balance",
        "admin_change_subscription"
      ]
    },
    "settings": {
      "viewMethods": [
        "hash",
        "version"
      ],
      "changeMethods": [
        "new",
        "deploy",
        "update",
        "get",
        "remove",
        "update_settings"
      ]
    },
    "user_settings": {
      "viewMethods": [
        "version"
      ],
      "changeMethods": [
        "new",
        "update",
        "get",
        "remove"
      ]
    }
  }
}