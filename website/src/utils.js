import { config } from '../../server/config'

export function getContractIds(env) {
  return env ? config[env] : config['development']
}

// Get contract ABIs
export function getContractAbi(key) {
  return config.abis[key]
}