import * as nearApi from 'near-api-js'

function getConfigByType(networkId, config) {
  return {
    networkId,
    nodeUrl: `https://rpc.${networkId}.near.org`,
    explorerUrl: `https://explorer.${networkId === 'mainnet' ? '' : networkId + '.'}near.org`,
    walletUrl: `https://wallet.${networkId === 'mainnet' ? '' : networkId + '.'}near.org`,
    helperUrl: `https://helper.${networkId}.near.org`,
    ...config,
  }
}

// Optional config, can be passed in before plugin install:
// {
//   appTitle: '',
//   contractName: '',
// }
export function getConfig(env, options = {}) {
  switch (env) {
    case 'production':
    case 'mainnet':
      return getConfigByType('mainnet', options)
    case 'development':
    case 'testnet':
      return getConfigByType('testnet', options)
    case 'betanet':
      return getConfigByType('betanet', options)
    case 'guildnet':
      return getConfigByType('guildnet', options)
    case 'local':
      return {
        ...options,
        networkId: 'local',
        nodeUrl: 'http://localhost:3030',
        walletUrl: 'http://localhost:4000/wallet',
      }
    default:
      throw Error(`Unconfigured environment '${env}'. Can be configured in src/config.js.`)
  }
}

// string to uint array
// REF: https://coolaj86.com/articles/unicode-string-to-a-utf-8-typed-array-buffer-in-javascript/
function unicodeStringToTypedArray(s) {
  const escstr = encodeURIComponent(s)
  const binstr = escstr.replace(/%([0-9A-F]{2})/g, function (match, p1) {
    return String.fromCharCode('0x' + p1)
  });
  let ua = new Uint8Array(binstr.length)
  Array.prototype.forEach.call(binstr, function (ch, i) {
    ua[i] = ch.charCodeAt(0)
  })
  return ua
}

export class NearPlugin {
  constructor(env, config) {
    // loading via CDN, requires adding this line to index.html:
    // <script src="https://cdn.jsdelivr.net/gh/nearprotocol/near-api-js/dist/near-api-js.js" ></script>
    if (!nearApi) return
    this.nearApi = { ...nearApi }
    this.config = getConfig(env, config)
    this.near = null
    this.provider = null
    this.keystore = null
    this.user = null

    return this
  }
  
  async loadNearProvider() {
    this.keystore = new this.nearApi.keyStores.BrowserLocalStorageKeyStore(window.localStorage, 'nearlib:keystore:')
    this.near = await this.nearApi.connect(Object.assign({ deps: { keyStore: this.keystore } }, this.config))
    this.provider = this.near.connection.provider
    return this
  }

  async loadAccount() {
    // Needed to access wallet
    this.walletConnection = new this.nearApi.WalletConnection(this.near)
    this.user = new this.nearApi.WalletAccount(this.near)

    if (this.walletConnection.getAccountId()) {
      this.user.accountId = this.walletConnection.getAccountId()
      this.user.balance = (await this.walletConnection.account().state()).amount
    }

    return this
  }

  async loginAccount() {
    if (this.user && this.user.isSignedIn()) return this.user
    const appTitle = this.config.appTitle || 'NEAR'
    await this.user.requestSignIn(this.config.pingbox, appTitle)

    // returns full access key?! CAREFUL!
    // await this.user.requestSignIn('')

    // re-load account
    return this.loadAccount()
  }

  async logoutAccount() {
    this.walletConnection = new this.nearApi.WalletConnection(this.near)
    this.user = new this.nearApi.WalletAccount(this.near)
    await this.user.signOut()

    this.keystore = null
    this.user = null
  }

  async getContractInstance(contract_id, abiMethods) {
    if (!this.user || !this.user.accountId) return
    const account = await this.walletConnection.account()
    const abi = {
      changeMethods: [],
      viewMethods: [],
      ...abiMethods,
    }

    // Sender is the account ID to initialize transactions.
    return new this.nearApi.Contract(
      account,
      contract_id,
      { ...abi, sender: account.accountId }
    )
  }

  async getAccountInstance(account_id) {
    return new this.nearApi.Account(this.near.connection, account_id)
  }

  async getTxData(hash, accountId) {
    if (!this.provider) return Promise.resolve()
    try {
      const result = await this.provider.txStatus(hash, accountId)
      // console.log("Result: ", result);
      return result
    } catch (e) {
      console.error(e);
    }
  }

  async getSignedTx({fnMethod, fnArgs, fnGas, fnAmount}, contractId) {
    if (!this.user || !this.user.accountId) return
    const txSender = this.user.accountId
    const content = await this.keystore.getKey(this.config.networkId, this.user.accountId)
    console.log('content', content);
    const keyPair = this.nearApi.KeyPairEd25519(content.secretKey);
    console.log('keyPair', keyPair);
    // Get the sender public key
    const publicKey = keyPair.getPublicKey();
    console.log("Sender public key:", publicKey.toString())

    // Get the public key information from the node
    const accessKey = await this.provider.query(
      `access_key/${txSender}/${publicKey.toString()}`, ""
    );
    console.log("Sender access key:", accessKey);

    // // TODO: Check to make sure provided key is a full access key
    // if (accessKey.permission !== "FullAccess") {
    //   return console.log(`Account [${txSender}] does not have permission to send tokens using key: [${publicKey}]`);
    // };

    // Each transaction requires a unique number or nonce
    // This is created by taking the current nonce and incrementing it
    const nonce = ++accessKey.nonce;
    console.log("Calculated nonce:", nonce);

    // Construct actions that will be passed to the createTransaction method below
    const actions = [];

    if (fnMethod === 'transfer') actions.push(near.transactions.transfer(fnAmount))
    else actions.push(this.nearApi.transactions.functionCall(fnMethod, fnArgs, fnGas, fnAmount))

    // Convert a recent block hash into an array of bytes.
    // This is required to prove the tx was recently constructed (within 24hrs)
    const recentBlockHash = this.nearApi.utils.serialize.base_decode(accessKey.block_hash);

    // Create a new transaction object
    const transaction = this.nearApi.transactions.createTransaction(
      txSender,
      publicKey,
      contractId,
      nonce,
      actions,
      recentBlockHash
    );

    // Before we can sign the transaction we must perform three steps
    // 1) Serialize the transaction in Borsh
    const serializedTx = this.nearApi.utils.serialize.serialize(
      near.transactions.SCHEMA,
      transaction
    );

    // 2) Hash the serialized transaction using sha256
    const serializedTxHash = new Uint8Array(sha256.array(serializedTx));

    // 3) Create a signature using the hashed transaction
    const signature = keyPair.sign(serializedTxHash);

    // Sign the transaction
    const signedTransaction = new this.nearApi.transactions.SignedTransaction({
      transaction,
      signature: new this.nearApi.transactions.Signature({
        keyType: transaction.publicKey.keyType,
        data: signature.signature
      })
    });

    // Send the transaction
    try {
      const result = await this.provider.sendTransaction(signedTransaction);

      console.log("Creation result:", result.transaction);
      console.log("----------------------------------------------------------------");
      console.log("OPEN LINK BELOW to see transaction in NEAR Explorer!");
      console.log(`${options.explorerUrl}/transactions/${result.transaction.hash}`);
      console.log("----------------------------------------------------------------");

      setTimeout(async function () {
        console.log("Checking transaction status:", result.transaction.hash);

        const status = await this.provider.sendJsonRpc("tx", [result.transaction.hash, options.accountId]);
        console.log("Transaction status:", status);
      }, 5000);
    }
    catch (error) {
      console.log("ERROR:", error);
    }
  }

  async getSignedPayload(message) {
    if (!this.user || !this.user.accountId) return
    const { secretKey } = await this.keystore.getKey(this.config.networkId, this.user.accountId)
    if (!secretKey) return
    const pair = new this.nearApi.utils.key_pair.KeyPairEd25519(secretKey)
    if (!pair) return
    const parsed = unicodeStringToTypedArray(message)
    const sig = await pair.sign(parsed)

    return {
      message,
      signature: sig.signature,
      publicKey: sig.publicKey.data,
    }
  }

  signedPayloadToString(payload) {
    if (!payload) return ''
    const payloadStr = `${payload.message}|${payload.signature.toString()}|${payload.publicKey.toString()}`
    return this.nearApi.utils.serialize.base_encode(payloadStr)
  }

}

// Register NEAR plugin with Vue
export default {
  install: async (app, { env, config }) => {
    // Add global context and methods for NEAR
    app.config.globalProperties.$near = await new NearPlugin(env, config)
    await app.config.globalProperties.$near.loadNearProvider()
    await app.config.globalProperties.$near.loadAccount()

    app.provide('$user', app.config.globalProperties.$near.user)

    // TODO: Add convenience methods for VUEX
  },
}