import Home from './views/Home.vue'
import Docs from './views/Docs.vue'
import User from './views/User.vue'
import Apps from './views/Apps.vue'
import Login from './views/Login.vue'
import Account from './views/Account.vue'
import Billing from './views/Billing.vue'
import NotificationSettings from './views/NotificationSettings.vue'
import Integrations from './views/Integrations.vue'
import Onboarding from './views/Onboarding.vue'
import NotFound from './views/NotFound.vue'
import store from './store'

// export const routeHandler = (to, from, next) => {
//   console.log('------------------------');
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     console.log('AAAAA');
//     if (store.getters.isAuthenticated(store.state)) {
//       console.log('BBBBBB');
//       next()
//       return
//     }
//     console.log('CCCCC');
//     // next({ path: '/login', replace: true })
//     return next({ name: 'Login' })
//   } else next()
// }

export let routes = [
  { path: '/', component: Home, name: 'Home' },
  { path: '/docs', component: Docs, name: 'Docs' },
  { path: '/onboarding', component: Onboarding, name: 'Onboarding', meta: { requiresAuth: true } },
  { path: '/login', component: Login, name: 'Login' },

  {
    path: '/user', component: User, name: 'User',
    meta: { requiresAuth: true },
    children: [
      { path: 'account', component: Account, name: 'Account' },
      { path: 'billing', component: Billing, name: 'Billing' },
      { path: 'notification-settings', component: NotificationSettings, name: 'Notification Settings' },
      { path: 'integrations', component: Integrations, name: 'Integrations' },
      { path: 'apps', component: Apps, name: 'Apps' },
    ],
  },

  { path: '/:path(.*)', component: NotFound }
]
