// Allow async without adding more babel overhead
import 'regenerator-runtime/runtime'

import { createApp } from 'vue'
import { createStore } from 'vuex'
import { createRouter, createWebHistory } from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import './tailwind.css'
import App from './App.vue'
import { routes, routeHandler } from './routes.js'
import storage from './store'
import near from './plugins/near'
import { config } from '../../server/config.js'

// const env = process.env.NODE_ENV || 'development'

const getEnvFromHost = () => {
  const host = window.location.host

  if (host === 'pingbox.app') return 'production'
  if (host === 'www.pingbox.app') return 'production'
  if (host === 'guildnet.pingbox.app') return 'guildnet'

  return 'development'
}

const env = getEnvFromHost()
const app = createApp(App)
const store = createStore(storage)
const router = createRouter({
  history: createWebHistory(),
  routes: import.meta.hot ? [] : routes,
  scrollBehavior(to) {
    if (to.hash) return { el: to.hash, behavior: 'smooth' }
    return { top: 0 }
  },
  beforeEach(to, from, next) {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters.isAuthenticated(store.state)) {
        next()
        return
      }
      next({ path: '/login', replace: true })
      // return next({ name: 'Login' })
    } else next()
  },
})

if (import.meta.hot) {
  let removeRoutes = []

  for (let route of routes) {
    removeRoutes.push(router.addRoute(route))
  }

  import.meta.hot.accept('./routes.js', ({ routes }) => {
    for (let removeRoute of removeRoutes) removeRoute()
    removeRoutes = []
    for (let route of routes) {
      removeRoutes.push(router.addRoute(route))
    }
    router.replace('')
  })
}

// router.beforeEach(routeHandler)

app.use(router)
app.use(store)
app.use(VueAxios, axios)
app.use(near, {
  env,
  config: {
    appTitle: '📨 Pingbox',
    ...config[env],
    abis: config.abis,
  },
})

app.mount('#app')
