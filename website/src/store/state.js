import { getContractIds } from '../utils'
const env = process.env.NODE_ENV
const contractIds = getContractIds(env)

export default {
  // Global
  gqlUrl: '',

  // User
  isAuthenticated: false,
  public_key: null,
  account_id: null,
  account: null,
  connection: null,

  // Contracts
  nearConfig: null,
  pingbox_contract: null,
  pingbox_contract_id: contractIds.pingbox,
  settings_contract: null,
  settings_contract_id: contractIds.settings,
  user_settings_contract: null,
  user_settings_contract_id: contractIds.user_settings,
}
