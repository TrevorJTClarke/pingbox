export default {
  // Global
  gqlUrl: () => {
    let url = 'http://localhost:2222'
    const host = window.location.host
    
    if (host === 'pingbox.app') url = 'https://api.pingbox.app'
    if (host === 'www.pingbox.app') url = 'https://api.pingbox.app'
    if (host === 'testnet.pingbox.app') url = 'https://testnet-api.pingbox.app'
    if (host === 'guildnet.pingbox.app') url = 'https://guildnet-api.pingbox.app'

    return `${url}/graphql`
  },

  // User
  isAuthenticated: state => state.isAuthenticated,
  public_key: state => state.public_key,
  account_id: state => state.account_id,
  account: state => state.account,
  connection: state => state.connection,

  // Contracts
  nearConfig: state => state.nearConfig,
  pingbox_contract: state => state.pingbox_contract,
  pingbox_contract_id: state => state.pingbox_contract_id,
  settings_contract: state => state.settings_contract,
  settings_contract_id: state => state.settings_contract_id,
  user_settings_contract: state => state.user_settings_contract,
  user_settings_contract_id: state => state.user_settings_contract_id,
}
