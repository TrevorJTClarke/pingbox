export default {
  // Generic Update
  update({ commit }, { key, value }) {
    commit('UPDATE', { key, value })
  },

  pushItem({ commit }, { key, value }) {
    commit('PUSH', { key, value })
  },

  deleteItem({ commit }, { key, idx }) {
    commit('DELETE', { key, idx })
  },

  addSubItem({ commit }, { key, subKey, value }) {
    commit('ADDSUBITEM', { key, subKey, value })
  },

  removeSubItem({ commit }, { key, subKey }) {
    commit('REMOVESUBITEM', { key, subKey })
  },

  setApiKey({ commit }, apiKey) {
    commit('UPDATE', { key: 'apiKey', value: apiKey })
  },

  logout({ commit }) {
    commit('UPDATEALL', {
      isAuthenticated: false,
      public_key: null,
      account_id: null,
      account: null,
      connection: null,
      nearConfig: null,
      contract: null,
    })
  },

  instantiateContracts({ commit }) {
    commit('UPDATE', { key: 'pingbox_contract', value: '' })
    commit('UPDATE', { key: 'settings_contract', value: '' })
    commit('UPDATE', { key: 'user_settings_contract', value: '' })
  },
}
