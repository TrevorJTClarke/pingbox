export const AlertConfig = `{
  uuid
  active
  app_id
  network
  type
  title
  desc
  category
  account_ids
  email
  inbox
  rocketchat
  slack
  sms
}`

export const UserSettings = `{
  user_id
  user_referral_id
  sub_account_ids
  subscription_contract_id
  email
  sms
  timezone
  currency
  productInfoActive
  rocketChatActive
  rocketChatChannel
  rocketChatDomain
  rocketChatToken
  slackActive
  slackChannel
  slackToken
  alerts ${AlertConfig}
}`

export const UserTransactions = `{
  hash
  amount
  status
  type
  timestamp
  title
}`

export const UserRewards = `{
  account_id
  amount
}`

export const Test = `{
  type
}`

export const GetUserSettings = `query GetUserSettings($user_id: String){
  userSettings(user_id: $user_id) ${UserSettings}
}`

export const GetUserRewards = `query GetUserRewards($user_id: String){
  rewards(user_id: $user_id) ${UserRewards}
}`

export const GetUserTransactions = `query GetUserTransactions($user_id: String){
  transactions(user_id: $user_id) ${UserTransactions}
}`

export const CreateUser = `mutation CreateUser(
  $user_id: String
  $account_id: String
  $data_hash: String
  $email: String
  $sms: String
  $timezone: String
  $currency: String
) {
  createUser(
    user_id: $user_id
    account_id: $account_id
    data_hash: $data_hash
    email: $email
    sms: $sms
    timezone: $timezone
    currency: $currency
  ) ${UserSettings}
}`

export const VerifyUser = `mutation VerifyUser(
  $user_id: String
  $user_referral_id: String
  $subscription_contract_id: String
  $verify_code: String
) {
  verifyUser(
    user_id: $user_id
    user_referral_id: $user_referral_id
    subscription_contract_id: $subscription_contract_id
    verify_code: $verify_code
  ) ${UserSettings}
}`

export const UpdateUserSettings = `mutation UpdateUserSettings(
  $user_id: String
  $sub_account_ids: [String]
  $subscription_contract_id: String
  $user_referral_id: String
  $email: String
  $sms: String
  $timezone: String
  $currency: String
  $productInfoActive: Boolean
  $rocketChatActive: Boolean
  $rocketChatChannel: String
  $rocketChatDomain: String
  $rocketChatToken: String
  $slackActive: Boolean
  $slackChannel: String
  $slackToken: String
) {
  updateUserSettings(
    user_id: $user_id
    sub_account_ids: $sub_account_ids
    subscription_contract_id: $subscription_contract_id
    user_referral_id: $user_referral_id
    email: $email
    sms: $sms
    timezone: $timezone
    currency: $currency
    productInfoActive: $productInfoActive
    rocketChatActive: $rocketChatActive
    rocketChatChannel: $rocketChatChannel
    rocketChatDomain: $rocketChatDomain
    rocketChatToken: $rocketChatToken
    slackActive: $slackActive
    slackChannel: $slackChannel
    slackToken: $slackToken
  ) ${UserSettings}
}`

export const UpdateUserAlerts = `mutation UpdateUserAlerts(
  $uuid: String
  $user_id: String
  $app_id: String
  $network: String
  $type: String
  $sub_account_ids: [String]
  $active: Boolean
  $email: Boolean
  $inbox: Boolean
  $rocketchat: Boolean
  $slack: Boolean
  $sms: Boolean
) {
  updateUserAlert(
    uuid: $uuid
    user_id: $user_id
    app_id: $app_id
    network: $network
    type: $type
    sub_account_ids: $sub_account_ids
    active: $active
    email: $email
    inbox: $inbox
    rocketchat: $rocketchat
    slack: $slack
    sms: $sms
  ) ${AlertConfig}
}`

export const SendUserAlertTest = `mutation SendUserAlertTest(
  $user_id: String
  $type: String
) {
  sendUserAlertTest(
    user_id: $user_id
    type: $type
  ) ${Test}
}`

export const SendVerifyEmail = `mutation SendVerifyEmail(
  $user_id: String
  $email: String
) {
  sendVerifyEmail(
    user_id: $user_id
    email: $email
  ) ${Test}
}`

export const UpdateUserEvent = `subscription UpdateUserSettings {
  updateUserSettings ${UserSettings}
}`
