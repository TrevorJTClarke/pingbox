export const System = `{
  currencies
  timezones
}`

export const GetSystem = `query GetSystem {
  system ${System}
}`
