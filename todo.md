JAMES todo:
- create coupon? or referral link that accrues to metabuidl guild
- create dapp account, with sub settings on the per-user setup
- create dapp subscription type?

MAINNET TODO:
* [ ] Add defaults to UI AND upon success setup
* [ ] Recipient for finalized vote doesnt notify user (transfer approved, notify them) (check txn for embedded actions?)
- update artwork on website
- balance transfer send email is wrong, receive is fine
- update app icons
- RPC Processor:
  - retry logic for recent blocks
  - failover to other nodes
  - -32000] Server error: DB Not Found Error: BLOCK HEIGHT: 59222016
- double check ongoing detects new subscriptions

production contracts:
- manager_v1.pingbox.near
- user_settings_v1.prefs.near

USER SETTINGS: 
- deploy: 40Tgas
- update: 12Tgas
- get: 12Tgas
- remove: 12Tgas
- delete account: 13Tgas


one near:
1000000000000000000000000

TODO:
* [ ] Billing - txn paid not showing (wrong user id hash)
* [ ] Fix TODO:s
- understanding setting notifications and integrations is hard for new user
- support currency setting for user value translation
- TEST: user reward based on referral ID
- setup staging & prod DBs (or just tables?)
- Setup URL in emails to edit setting link
- queue: DB
  - For unstaking complete - have the item go back into queue until epoch has passed
- Comb for TODO:s
  - use on-chain config loaded at server start for subscription payment and referral reward amt
- "watcher" account app - user can add apps that configure to diff alerting scenarios like whale alerts


Onboarding Flow:
1. Accept login / user is logged in
2. Payment:
  - send email via API (with keccak of user account_id)
  3. Send 12 N (why 13?! needs fees taken into consideration)
  4. add_subscription (hash)
5. Deploy Settings contract (pay how much?)
  6. initialize with email hash (data hash)
7. Indexer wait for confirmation of data hash
8. send email for validation
9. user clicks link for validation,
  - trigger allowance to subscription contract: validate_subscription
  - gets referral code
  - submits to DB
10. User can resend email validation -- API checks to make sure data hash is sync'd first, debounce!!!
11. User can send test email (1x)
12. DONE! :D



Server & Website needs:
- server: 
    api.pingbox.app - mainnet
    testnet-api.pingbox.app - testnet
    mg.pingbox.app
- website:
    pingbox.app - mainnet
    testnet.pingbox.app - testnet



DB: 
- users (meta, integrations, etc)
- users_alerts_configs (alert configs)
- users_alerts (triggered alerts)
- users_transactions (payments, rewards)
- subscriptions (users, dapps)
- analytics (on chain events)


Emails:
- Notifs
    - Balance transfer, root/subs
    - Staking (Unstaking is SUPER CRITICAL, going to be slashed?!)
    - Referral reward bonus
    - contract deployed
    - access key change
    - Function calls to new contracts
    - alert me if staking pool is losing seat
- Email template:
    - add referral code in each email
    - Contract Dynamic:
        - brandIcon: (via Nearvatars)
        - brandName: (contractId or specified)
        - brandColor: (hex or rgb)
        - theme: (light, dark)
        - type: (TBD)
        - title: 
        - message: (safe html string)
        - ctaTitle: 
        - ctaUrl:

NOTES:

Emails for dapps: could dapp register on behalf of user with simple notifs for email/etc? 
  - allow users to signup under a dapp subscription
  - require signing of some kind (account_id, email)
  - unsubscribe
  - upgrade to pingbox premium (referral rew goes to dapp)
  - check daily balances to alert dapps of low message balance
  - SDK for website email form
  - manual trigger for notifs

Docs: Setup gitbook?

Near Alerter - Send me intelligent alerts/emails based on my transaction history
  - User pays to subscribe to this service, submitting email, balance (user pays a little extra until email is verified)
    - verification stored on chain? for DID reference?
    - create sub-account for storing prefs contract
  - Chain stores payment amount, keccak of email, account ID that paid
  - User confirms email, access key is pre-paid for user to update to activated if hash matched (hash token derived by email keccak and block index, sent via alert server service, hash verified by combining and hashing data on chain)
  - pingbox.near
  - User pays for each alert configuration? (user must be able to sign to change a setting, but free)
  - referal code reward? (part of balance paid for subscription goes to referrer)
  - login/logout/pay with near wallet
  - dapp integration SDK? cross contract call, contract subscription
  - https://www.mailgun.com/pricing/
  - Email Types:
    - Balance transfer, root/subs
    - Staking (Unstaking is SUPER CRITICAL, going to be slashed?!)
    - contract deployed
    - access key change
    - Function calls to new contracts
    - Referral reward bonus
  - Email template:
    - add referral code in each email
  - Email Message Inbox - as part of subscription, user can choose a sub account to receive messages at, which is owned by their signing account
- FE UI for signup, settings
- full E2E testing
- other integrations:
    - slack, rocketchat, telegram, sms
    - status.im
    - matrix servers
    - webhook?
    - chain inbox
    - single READ message - short lived
    - discord bot
- Dapp SDK
    - log data with specific format
    - deployed config meta
      - per fn
      - per log/event
    - registers with main alerts contract & subscribes
    - does NOT keep track of users
    - pay by messages sent total, use escrow, proof is txn hash count merklized
- API
    - CRUD user settings
    - onboarding
- Deployment:
    - user settings contract
    - user subscription contract
    - dapp subscription contract (using ERC20 pay per msg)

## Events

```
SUB:NEW:t.testnet
SUB:VAL:t.testnet
SUB:UPD:t.testnet
SUB:REF:keccak256...
```

## TODO: Dapp event logs with message payloads

import StorageProvider from './providers/storage'

## Deploy
near dev-deploy --wasmFile ./contracts/dist/pingbox.wasm
near deploy --wasmFile ./contracts/dist/pingbox.wasm --initFunction new --initArgs '{}' --accountId pingbox.testnet
near call __ --initFunction new --initArgs '{}'

## Commands

```
# user fns
near call __ add_subscription '{"hash": [0,1,2]}' --amount 12.5 --accountId t.testnet
near call __ validate_subscription '{"hash": [0,1,2]}' --accountId t.testnet
near call __ update_subscription '{"data_hash": [0,1,2,3,4], "active": true}' --accountId t.testnet
near view __ get_referral_code --accountId t.testnet

# admin fns
near call __ admin_update_fees '{"subscription_amount": 11, "subscription_verify_fee": 1, "referral_reward": 1}' --accountId t.testnet
near call __ admin_transfer_balance '{"account_id": "t.testnet", "amount": 10}' --accountId t.testnet
near call __ admin_pause_subscription '{"account_id": "fake001.t.testnet", "active": false}' --accountId t.testnet
near call __ admin_transfer_ownership '{"public_key": "Base58PublicKey"}' --accountId t.testnet
near view __ get_all_subscriptions --accountId t.testnet
near view __ get_subscription '{"account_id": "t.testnet"}' --accountId t.testnet
near view __ get_stats --accountId t.testnet
```

near call dev-1609449238857-6613339 add_subscription '{"hash": [0,1,2]}' --amount 12.5 --accountId t.testnet

near view dev-1609449238857-6613339 get_stats --accountId t.testnet
near view dev-1609449238857-6613339 get_all_subscriptions --accountId t.testnet
near view dev-1609449238857-6613339 get_subscription '{"account_id": "t.testnet"}' --accountId t.testnet



https://api.coingecko.com/api/v3/simple/price?ids=near&vs_currencies=usd%2Cjpy%2Cbtc%2Ceth%2Cxau%2Ceur




&str    -> String  | String::from(s) or s.to_string() or s.to_owned()
&str    -> &[u8]   | s.as_bytes()
&str    -> Vec<u8> | s.as_bytes().to_vec() or s.as_bytes().to_owned()
String  -> &str    | &s if possible* else s.as_str()
String  -> &[u8]   | s.as_bytes()
String  -> Vec<u8> | s.into_bytes()
&[u8]   -> &str    | s.to_vec() or s.to_owned()
&[u8]   -> String  | std::str::from_utf8(s).unwrap(), but don't**
&[u8]   -> Vec<u8> | String::from_utf8(s).unwrap(), but don't**
Vec<u8> -> &str    | &s if possible* else s.as_slice()
Vec<u8> -> String  | std::str::from_utf8(&s).unwrap(), but don't**
Vec<u8> -> &[u8]   | String::from_utf8(s).unwrap(), but don't**

