#!/bin/bash
# Uncomment the desired network
export NEAR_ENV=testnet
# export NEAR_ENV=mainnet

export FACTORY=testnet
# export FACTORY=near
# export FACTORY=registrar

export MASTER_ACCOUNT=pa.testnet
export PINGBOX_ACCOUNT_ID=pingbox1.$MASTER_ACCOUNT
export SETTINGS_ACCOUNT_ID=settings7.$MASTER_ACCOUNT

export TITLE_ACCOUNT_ID=pb_user00000001.testnet

# clear all accounts
near call $SETTINGS_ACCOUNT_ID delete_account '{}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000
near delete $PINGBOX_ACCOUNT_ID $MASTER_ACCOUNT
near delete $SETTINGS_ACCOUNT_ID $MASTER_ACCOUNT
# near delete $TITLE_ACCOUNT_ID $MASTER_ACCOUNT

echo "Clear Complete"