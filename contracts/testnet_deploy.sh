#!/bin/bash
# This file is used for starting a fresh set of all contracts & configs
set -e

if [ -d "res" ]; then
  echo ""
else
  mkdir res
fi

cd "`dirname $0`"

if [ -z "$KEEP_NAMES" ]; then
  export RUSTFLAGS='-C link-arg=-s'
else
  export RUSTFLAGS=''
fi

# build the things
cargo build --all --target wasm32-unknown-unknown --release
cp target/wasm32-unknown-unknown/release/*.wasm ./res/

# Uncomment the desired network
export NEAR_ENV=testnet
# export NEAR_ENV=mainnet

export FACTORY=testnet
# export FACTORY=near
# export FACTORY=registrar

export MASTER_ACCOUNT=pbx.testnet
export PINGBOX_ACCOUNT_ID=pingbox.$MASTER_ACCOUNT
export SETTINGS_ACCOUNT_ID=settings.$MASTER_ACCOUNT
# TODO: When ready: change to this
# export SETTINGS_ACCOUNT_ID=settings.testnet
export DAO_ACCOUNT_ID=dao.sputnikv2.testnet

# export TITLE_ACCOUNT_ID=pb_user00000001.testnet
export TITLE_ACCOUNT_ID=nym.testnet
export TITLE_PK=ed25519:6Mzi9dRMSiPWYp7BgLJ2Lj6KPCcs48FwB93NgQ4LKSBo

# near delete $PINGBOX_ACCOUNT_ID $MASTER_ACCOUNT
# near delete $SETTINGS_ACCOUNT_ID $MASTER_ACCOUNT

# create all accounts
# near create-account $PINGBOX_ACCOUNT_ID --masterAccount $MASTER_ACCOUNT
# near create-account $SETTINGS_ACCOUNT_ID --masterAccount $MASTER_ACCOUNT

# Deploy all the contracts to their rightful places
# near deploy --wasmFile res/pingbox.wasm --initFunction new --initArgs '{"settings_contract": "'$SETTINGS_ACCOUNT_ID'"}' --accountId $PINGBOX_ACCOUNT_ID
# near view $PINGBOX_ACCOUNT_ID version
# near deploy --wasmFile res/settings_pingbox.wasm --initFunction new --initArgs '{"dao": "'$DAO_ACCOUNT_ID'"}' --accountId $SETTINGS_ACCOUNT_ID
# near view $SETTINGS_ACCOUNT_ID version

near call $SETTINGS_ACCOUNT_ID init '{}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

echo "Testnet Deploy Complete"