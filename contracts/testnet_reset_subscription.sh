#!/bin/bash
# This file is used for starting a fresh set of all contracts & configs
set -e

if [ -d "res" ]; then
  echo ""
else
  mkdir res
fi

cd "`dirname $0`"

if [ -z "$KEEP_NAMES" ]; then
  export RUSTFLAGS='-C link-arg=-s'
else
  export RUSTFLAGS=''
fi

# build the things
cargo build --all --target wasm32-unknown-unknown --release
cp target/wasm32-unknown-unknown/release/*.wasm ./res/

# Uncomment the desired network
export NEAR_ENV=testnet
# export NEAR_ENV=mainnet

export FACTORY=testnet
# export FACTORY=near
# export FACTORY=registrar

export MASTER_ACCOUNT=pbx.testnet
export PINGBOX_ACCOUNT_ID=pingbox.$MASTER_ACCOUNT
export SETTINGS_ACCOUNT_ID=settings.$MASTER_ACCOUNT
# TODO: When ready: change to this
# export SETTINGS_ACCOUNT_ID=settings.testnet
export DAO_ACCOUNT_ID=dao.sputnikv2.testnet

# export TITLE_ACCOUNT_ID=pb_user00000001.testnet
export TITLE_ACCOUNT_ID=price.testnet

# Call all the places to remove the subscription fully
near call $PINGBOX_ACCOUNT_ID admin_change_subscription '{"id": "'$TITLE_ACCOUNT_ID'", "active": false, "remove": true}' --accountId $PINGBOX_ACCOUNT_ID --gas 300000000000000
near call $SETTINGS_ACCOUNT_ID delete_account '{}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

echo "Testnet Subscription Reset Complete"