Pingbox Settings Contract:
- Mapping: <AccountId, ContractHashId>
- Deploy to USER_HASH.settings_pingbox.near
- Get setting by AccountId, ContractSettingId (cross contract view)
- Get hash by string
- withdraw - Optional, for owner?

## Contracts:
bingpox.testnet
tings_bingpox.testnet
hu0001.tings_bingpox.testnet
hu0001_tings_bingpox.testnet

## Commands

```
# user fns
near deploy --wasmFile res/settings_pingbox.wasm --initFunction new --initArgs '{}' --accountId YOUR_ACCOUNT.testnet --amount 1
near call settings.testnet deploy '{}' --accountId user_account.testnet --amount 1
near view dev-1610236423337-9565516 hash '{"s":"dapp_a_1"}'
near call dev-1610236423337-9565516 deploy --accountId dev-1610236423337-9565516
near call dev-1610236423337-9565516 get '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647"}' --accountId dev-1610236423337-9565516
near call dev-1610236423337-9565516 update '{"k":"dapp_a_1"}' --accountId t.testnet
near call dev-1610236423337-9565516 remove '{"k":"dapp_a_1"}' --accountId t.testnet
```


near call dev-1610236423337-9565516 up '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647","v":"6273151f959616268004b58dbb21e5c851b7b8d04498b4aabee12291d22fc034"}' --accountId dev-1610236423337-9565516