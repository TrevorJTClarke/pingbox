use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::LookupMap;
use near_sdk::{
    env, ext_contract, log, near_bindgen, AccountId, Balance, BorshStorageKey, Gas, PanicOnDefault,
    Promise, StorageUsage,
};

near_sdk::setup_alloc!();

/// Basic configs
pub const ONE_NEAR: u128 = 1_000_000_000_000_000_000_000_000;
pub const ALLOWANCE:   u128 = 20_000_000_000_000_000_000_000;

/// Gas & Balance Configs
pub const NO_DEPOSIT: u128 = 0;
pub const GAS_FOR_DEPLOY_CALLBACK: Gas = 25_000_000_000_000;
pub const GAS_FOR_INIT_CALL: Gas = 15_000_000_000_000;
pub const GAS_FOR_UPDATE_CALL: Gas = 30_000_000_000_000;
pub const GAS_FOR_REMOVE_CALL: Gas = 30_000_000_000_000;
pub const GAS_FOR_GET_CALL: Gas = 30_000_000_000_000;

#[derive(BorshStorageKey, BorshSerialize)]
pub enum StorageKeys {
    UserContracts,
    UserSettings,
}

#[ext_contract(ext_userset)]
pub trait UserSettings {
    fn new() -> Self;
    fn dead(&mut self);
    fn update(&mut self, k: String, v: String);
    fn remove(&mut self, k: String);
    fn get(&self, k: String) -> Option<String>;
}

#[ext_contract(ext_self)]
pub trait Contract {
    fn on_deploy(id: AccountId);
}

// SettingsPingbox is an ownerless contract that serves as the middle
#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize, PanicOnDefault)]
pub struct SettingsPingbox {
    pub base_storage_usage: StorageUsage,
    pub user_storage_usage: StorageUsage,

    // DAO Ownership, etc
    dao: AccountId,

    // Mapping: <AccountId, AccountContractHashId>
    user_contracts: LookupMap<AccountId, String>,

    // ONLY used for calculating user contract storage fees.
    settings: LookupMap<String, String>,
}

#[near_bindgen]
impl SettingsPingbox {
    /// This contract / account should not have any owners, however it CAN have DAO ownership that manages updates, etc.
    /// ```bash
    /// near deploy --wasmFile res/settings_pingbox.wasm --initFunction new --initArgs '{"dao": "dao.sputnik.testnet"}' --accountId settings_pingbox.testnet
    /// ```
    #[init]
    pub fn new(dao: Option<AccountId>) -> Self {
        assert!(!env::state_exists(), "The contract is already initialized");
        assert_eq!(
            env::current_account_id(),
            env::predecessor_account_id(),
            "Must be called by owner"
        );

        let mut this = SettingsPingbox {
            base_storage_usage: 0,
            user_storage_usage: 0,
            dao: dao.expect("DAO ownership must be specified"),
            user_contracts: LookupMap::new(StorageKeys::UserContracts),
            settings: LookupMap::new(StorageKeys::UserSettings),
        };

        // compute storage needs before finishing
        this.measure_account_storage_usage();

        this
    }

    /// Measure the storage an agent will take and need to provide
    fn measure_account_storage_usage(&mut self) {
        let initial_storage_usage = env::storage_usage();

        // calculate storage for THIS contract usage
        let tmp_str = "z".repeat(64);
        self.user_contracts.insert(&tmp_str, &tmp_str);
        self.base_storage_usage = env::storage_usage() - initial_storage_usage;
        self.user_contracts.remove(&tmp_str);

        // Calculate the storage balance needed for user contract
        let tmp_str = "z".repeat(160); // real val is 143
        self.settings.insert(&tmp_str, &tmp_str);
        // adding 15x to assume there will be an avg 15 settings stored
        self.user_storage_usage = (env::storage_usage() - initial_storage_usage) * 15;
        self.settings.remove(&tmp_str);
    }

    // deploys a new settings contract for an account, must be signer!
    // LOGIC:
    // 1. Create a new sub account, based on the truncated hash value of account id
    // 2. add function access key to the signer
    // 3. deploy the user_settings contract to new account
    ///
    /// ```bash
    /// near call settings.testnet deploy '{}' --accountId user_account.testnet --amount 1
    /// ```
    #[payable]
    pub fn deploy(&mut self) -> Promise {
        assert!(
            self.user_contracts.get(&env::predecessor_account_id()).is_none(),
            "User already has contract"
        );
        assert!(
            env::attached_deposit() > ALLOWANCE + u128::from(self.user_storage_usage) + u128::from(self.base_storage_usage),
            "User storage balance not paid"
        );

        // create a hash of the account
        let h = self.hash(env::predecessor_account_id());

        // trim hash to shortened verison
        let sub_account_id = format!("{}.{}", &h[3..18], env::current_account_id());
        log!("New_Account: {}", &sub_account_id);

        // store the reference
        self.user_contracts
            .insert(&env::predecessor_account_id(), &h[3..18].to_string());

        // Deploy the settings contract to a new sub account
        // NOTE: access key restricts user tampering, and forces THIS contract to manage.
        Promise::new(sub_account_id.clone())
            .create_account()
            .transfer(env::attached_deposit() - u128::from(self.base_storage_usage))
            .add_access_key(
                env::signer_account_pk(),
                Balance::from(ALLOWANCE),
                env::predecessor_account_id(),
                b"new,update,remove,dead".to_vec(),
            )
            .deploy_contract(include_bytes!("../../res/user_settings_pingbox.wasm").to_vec())
            .then(
                ext_self::on_deploy(
                    sub_account_id,
                    &env::current_account_id(),
                    NO_DEPOSIT,
                    GAS_FOR_DEPLOY_CALLBACK
                )
            )
    }

    // trigger init on user contract via callback from deploy
    #[private]
    pub fn on_deploy(&mut self, id: AccountId) -> String {
        ext_userset::new(&id, NO_DEPOSIT, GAS_FOR_INIT_CALL);
        id
    }

    /// Initialize the user setting contract
    ///
    /// ```bash
    /// near call settings.testnet init '{}' --accountId user_account.testnet
    /// ```
    pub fn init(&mut self) -> Promise {
        let acct = self
            .sub_account(env::predecessor_account_id())
            .expect("No user account found");

        ext_userset::new(&acct, NO_DEPOSIT, GAS_FOR_INIT_CALL)
    }

    // deletes a settings sub contract
    ///
    /// ```bash
    /// near call settings.testnet delete_account '{}' --accountId user_account.testnet
    /// ```
    pub fn delete_account(&mut self) -> Promise {
        assert!(
            self.user_contracts.get(&env::signer_account_id()).is_some(),
            "User doesnt exist"
        );

        // create a hash of the account
        let h = self.hash(env::signer_account_id());

        // trim hash to shortened verison
        let sub_account_id = format!("{}.{}", &h[3..18], env::current_account_id());
        log!("Delete_Account: {}", &sub_account_id);

        // remove the reference
        self.user_contracts.remove(&env::signer_account_id());

        // Fully delete the settings contract to a new sub account
        ext_userset::dead(&sub_account_id, NO_DEPOSIT, GAS_FOR_INIT_CALL)
    }

    /// Update a specific setting on the user setting contract
    ///
    /// ```bash
    /// near call settings.testnet update '{"k": "fdjsfljds", "v": "fdjsfljds"}' --accountId user_account.testnet --amount 0.001
    /// ```
    #[payable]
    pub fn update(&mut self, k: String, v: String) -> Promise {
        let acct = self
            .sub_account(env::predecessor_account_id())
            .expect("No user account found");

        ext_userset::update(k, v, &acct, env::attached_deposit(), GAS_FOR_UPDATE_CALL)
    }

    /// Remove a setting for a user
    ///
    /// ```bash
    /// near call settings.testnet remove '{"k": "fdjsfljds", "v": "fdjsfljds"}' --accountId user_account.testnet
    /// ```
    pub fn remove(&mut self, k: String) -> Promise {
        let acct = self
            .sub_account(env::predecessor_account_id())
            .expect("No user account found");

        ext_userset::remove(k, &acct, env::attached_deposit(), GAS_FOR_REMOVE_CALL)
    }

    /// get a specific setting for a user
    ///
    /// ```bash
    /// near view settings.testnet get '{"k": "fdjsfljds"}'
    /// ```
    pub fn get(&mut self, id: AccountId, k: String) -> Promise {
        let acct = self.sub_account(id).expect("No user account found");

        ext_userset::get(k, &acct, NO_DEPOSIT, GAS_FOR_GET_CALL)
    }

    // Helper: Returns hashified string used in all other variables
    // TYPE: INTERNAL ONLY
    fn sub_account(&self, id: AccountId) -> Option<String> {
        let acct = self.user_contracts.get(&id).expect("No user account found");
        Some(format!("{}.{}", &acct, env::current_account_id()))
    }

    /// Helper: Returns hashified string used in all other variables
    ///
    /// ```bash
    /// near view settings.testnet version
    /// ```
    pub fn hash(&self, s: String) -> String {
        // generate & return hash from string
        let i = env::sha256(s.as_bytes());
        let key: Vec<String> = i.iter().map(|b| format!("{:02x}", b)).collect();
        key.join("")
    }

    /// change the contract basic parameters, in case of needing to upgrade
    /// or change to different account IDs later.
    /// Can only be called by the DAO contract (if originally configured)
    ///
    /// ```bash
    /// near call settings.testnet update_settings '{"dao": "dao.sputnik.testnet"}' --accountId dao.sputnik.testnet
    /// ```
    pub fn update_settings(&mut self, dao: Option<AccountId>) {
        assert_eq!(
            self.dao.clone(),
            env::predecessor_account_id(),
            "Callee must be dao contract"
        );

        // Update each individual setting
        if dao.is_some() {
            self.dao = dao.unwrap();
        }
    }

    /// Returns semver of this contract.
    ///
    /// ```bash
    /// near view settings.testnet version
    /// ```
    pub fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }
}

// TODO: TESTS
// - can own
// - can deploy
// - can get setting
// - can hash

// #[cfg(not(target_arch = "wasm32"))]
// #[cfg(test)]
// mod tests {
//     use super::*;
//     use near_sdk::MockedBlockchain;
//     use near_sdk::{testing_env, VMContext};

//     fn get_context(input: Vec<u8>, is_view: bool) -> VMContext {
//         VMContext {
//             current_account_id: "alice_near".to_string(),
//             signer_account_id: "bob_near".to_string(),
//             signer_account_pk: vec![0, 1, 2],
//             predecessor_account_id: "carol_near".to_string(),
//             input,
//             block_index: 0,
//             block_timestamp: 0,
//             account_balance: 0,
//             account_locked_balance: 0,
//             storage_usage: 0,
//             attached_deposit: 0,
//             prepaid_gas: 10u64.pow(18),
//             random_seed: vec![0, 1, 2],
//             is_view,
//             output_data_receivers: vec![],
//             epoch_height: 0,
//         }
//     }

//     // #[test]
//     // fn get_nonexistent_message() {
//     //     let context = get_context(vec![], true);
//     //     testing_env!(context);
//     //     let contract = Thing::default();
//     //     assert_eq!(None, contract.set_thing("francis.near".to_string()));
//     // }
// }
