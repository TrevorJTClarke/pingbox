#!/bin/bash
# This file is used for starting a fresh set of all contracts & configs
set -e

if [ -d "res" ]; then
  echo ""
else
  mkdir res
fi

cd "`dirname $0`"

if [ -z "$KEEP_NAMES" ]; then
  export RUSTFLAGS='-C link-arg=-s'
else
  export RUSTFLAGS=''
fi

# build the things
cargo build --all --target wasm32-unknown-unknown --release
cp target/wasm32-unknown-unknown/release/*.wasm ./res/

# Uncomment the desired network
export NEAR_ENV=mainnet
export FACTORY=near

# needs balance of 2 NEAR
export MASTER_ACCOUNT=pingbox.near
# needs balance of 2 NEAR
export MASTER_PREFS_ACCOUNT=prefs.near
# needs balance of 6 NEAR
export PINGBOX_ACCOUNT_ID=manager_v1.$MASTER_ACCOUNT
# needs balance of 6 NEAR
export SETTINGS_ACCOUNT_ID=user_settings_v1.$MASTER_PREFS_ACCOUNT
export DAO_ACCOUNT_ID=prod.sputnik-dao.near

######
# NOTE: All commands below WORK, just have them off for safety.
######

# create all accounts
# near create-account $PINGBOX_ACCOUNT_ID --masterAccount $MASTER_ACCOUNT --initialBalance 2
# near create-account $SETTINGS_ACCOUNT_ID --masterAccount $MASTER_PREFS_ACCOUNT --initialBalance 2

# Deploy all the contracts to their rightful places
# near deploy --wasmFile res/pingbox.wasm --initFunction new --initArgs '{"settings_contract": "'$SETTINGS_ACCOUNT_ID'"}' --accountId $PINGBOX_ACCOUNT_ID
# near view $PINGBOX_ACCOUNT_ID version
# near deploy --wasmFile res/settings_pingbox.wasm --initFunction new --initArgs '{"dao": "'$DAO_ACCOUNT_ID'"}' --accountId $SETTINGS_ACCOUNT_ID
# near view $SETTINGS_ACCOUNT_ID version


# RE:Deploy all the contracts to their rightful places
# near deploy --wasmFile res/pingbox.wasm --accountId $PINGBOX_ACCOUNT_ID
# near view $PINGBOX_ACCOUNT_ID version
# near deploy --wasmFile res/settings_pingbox.wasm --accountId $SETTINGS_ACCOUNT_ID
# near view $SETTINGS_ACCOUNT_ID version

# near call $PINGBOX_ACCOUNT_ID admin_change_subscription '{"id": ".near", "active": false, "remove": true}' --accountId $PINGBOX_ACCOUNT_ID

echo "Mainnet Deploy Complete"