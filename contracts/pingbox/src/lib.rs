use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::UnorderedMap;
use near_sdk::json_types::{Base64VecU8, ValidAccountId, U128};
use near_sdk::{
    env, log, near_bindgen, ext_contract, AccountId, Balance, BlockHeight, BorshStorageKey, PanicOnDefault,
    Promise, StorageUsage, Gas,
};

near_sdk::setup_alloc!();

// Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa Ⓝa - Batmannnnnnnn
/// Basic configs
pub const ONE_NEAR:             u128 = 1_000_000_000_000_000_000_000_000;
pub const SETTINGS_DEPLOY_COST: u128 = 1_337_000_000_000_000_000_000_000;
pub const ACCESS_KEY_ALLOWANCE:     u128 = 1_000_000_000_000_000_000_000;

/// Gas & Balance Configs
pub const NO_DEPOSIT: u128 = 0;
pub const GAS_FOR_DEPLOY_CALL: Gas = 60_000_000_000_000;

#[derive(BorshStorageKey, BorshSerialize)]
pub enum StorageKeys {
    Subscriptions,
    Referals,
}

#[ext_contract(ext_settings)]
pub trait SettingsPingbox {
    fn deploy(&mut self) -> Promise;
    fn delete_account(&mut self) -> Promise;
    fn init(&mut self) -> Promise;
    fn get(&mut self, id: AccountId, k: String) -> Promise;
    fn update(&mut self, k: String, v: String) -> Promise;
    fn remove(&mut self, k: String) -> Promise;
    fn hash(&self, s: String) -> String;
}

#[derive(BorshDeserialize, BorshSerialize)]
pub struct Subscription {
    active: bool,
    verified: bool,
    subscribe_block: BlockHeight,
    balance: Balance,
    referral_code: String,
    account_hash: Vec<u8>,
    data_hash: Option<Vec<u8>>,
}

#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize, PanicOnDefault)]
pub struct Contract {
    pub base_storage_usage: StorageUsage,
    owner: AccountId,
    settings_contract: AccountId,

    subscriptions: UnorderedMap<AccountId, Subscription>,
    subscription_fee: u128,
    validation_reward: u128,
    referral_reward: u128,
    referrals: UnorderedMap<String, AccountId>,
}

#[near_bindgen]
impl Contract {
    /// Initialize an instance
    /// TODO: add migration logic
    ///
    /// ```bash
    /// near deploy --wasmFile res/pingbox.wasm --initFunction new --initArgs '{"settings_contract": "$SETTINGS_ACCOUNT_ID"}' --accountId pingbox_account.testnet
    /// ```
    #[init]
    pub fn new(settings_contract: AccountId) -> Self {
        // Make absolutely sure this contract doesnt get state removed easily
        assert!(!env::state_exists(), "The contract is already initialized");
        assert_eq!(
            env::current_account_id(),
            env::predecessor_account_id(),
            "Must be called by owner"
        );

        let mut this = Contract {
            base_storage_usage: 0,
            owner: env::predecessor_account_id(),
            settings_contract,
            subscription_fee: 3 * ONE_NEAR,
            validation_reward: ONE_NEAR / 200,
            referral_reward: ONE_NEAR / 2,
            referrals: UnorderedMap::new(StorageKeys::Referals),
            subscriptions: UnorderedMap::new(StorageKeys::Subscriptions),
        };
        // compute storage needs before finishing
        this.measure_account_storage_usage();
        this
    }

    /// Measure the storage an agent will take and need to provide
    fn measure_account_storage_usage(&mut self) {
        let initial_storage_usage = env::storage_usage();
        // Create a temporary, dummy entry and measure the storage used.
        let tmp_account_id = "z".repeat(64);
        self.subscriptions.insert(
            &tmp_account_id,
            &Subscription {
                active: true,
                verified: true,
                subscribe_block: env::block_index(),
                balance: Balance::from(env::block_timestamp()),
                referral_code: "z".repeat(64),
                account_hash: [9].repeat(64),
                data_hash: Some([9].repeat(64)),
            },
        );
        self.base_storage_usage = env::storage_usage() - initial_storage_usage;
        // Remove the temporary entry.
        self.subscriptions.remove(&tmp_account_id);
    }

    // require only owner
    fn only_owner(&self) {
        assert_eq!(
            self.owner,
            env::predecessor_account_id(),
            "Only owner can execute this fn"
        )
    }

    /// Add subscription
    ///
    /// ```bash
    /// near call _pingbox_account_ add_subscription '{"hash": "fjskfdsaf=", "rfr": "fsafsadfsafds"}' --accountId my_account.testnet --amount 12.5
    /// ```
    #[payable]
    pub fn add_subscription(&mut self, hash: Base64VecU8, rfr: Option<String>) -> String {
        // Require payment to enable subscription
        assert!(
            env::attached_deposit() > u128::from(self.base_storage_usage),
            "Attached deposit must be greater than base fees"
        );
        assert!(
            env::attached_deposit() >= self.subscription_fee,
            "Attached deposit must be greater than subscription fees"
        );

        // Require this is new subscription
        if self.subscriptions.get(&env::predecessor_account_id()).is_some() {
            panic!(
                "Account subscription already exists {}",
                &env::predecessor_account_id()
            );
        };

        // Remaining balance
        let remaining_balance = env::attached_deposit() - SETTINGS_DEPLOY_COST;

        let subscription = Subscription {
            active: true,
            verified: false,
            subscribe_block: env::block_index(),
            balance: remaining_balance,
            referral_code: format!("-"),
            account_hash: hash.0,
            data_hash: None,
        };

        self.subscriptions
            .insert(&env::predecessor_account_id(), &subscription);
        // User has referral code
        if rfr.clone().is_some() {
            let friend_id = self.referrals.get(&rfr.clone().unwrap());
            if friend_id.is_some() {
                Promise::new(friend_id.unwrap()).transfer(self.referral_reward);
                log!("SUB:REF:{}", &rfr.clone().unwrap());
            }
        }

        // Create access key for this contract, then deploy settings contract for user
        Promise::new(env::current_account_id()).add_access_key(
            env::signer_account_pk(),
            ACCESS_KEY_ALLOWANCE,
            env::predecessor_account_id(),
            b"validate_subscription".to_vec(),
        ).then(
            ext_settings::deploy(
                &self.settings_contract,
                SETTINGS_DEPLOY_COST,
                GAS_FOR_DEPLOY_CALL
            )
        );

        self.sub_account(env::predecessor_account_id())
    }

    /// Validate a subscription
    ///
    /// ```bash
    /// near call _pingbox_account_ validate_subscription '{"hash": "fjskfdsaf="}' --accountId my_account.testnet
    /// ```
    pub fn validate_subscription(&mut self, hash: Base64VecU8) -> String {
        let mut subscription = self
            .subscriptions
            .get(&env::predecessor_account_id())
            .expect("Must have a subscription");

        // Return the extra amount for incentivizing validation
        Promise::new(env::predecessor_account_id())
            .transfer(self.validation_reward);
        Promise::new(env::current_account_id())
            .delete_key(env::signer_account_pk());

        // generate & return referral code
        let input = format!(
            "{:?}{:?}{:?}",
            &env::predecessor_account_id(),
            hash.0,
            env::block_index()
        );

        // Update subscription with referral code
        subscription.referral_code = self.hash(input);
        subscription.balance = subscription.balance - self.validation_reward;
        subscription.verified = true;
        self.subscriptions
            .insert(&env::predecessor_account_id(), &subscription);
        self.referrals
            .insert(&subscription.referral_code, &env::predecessor_account_id());

        subscription.referral_code
    }

    /// Update if subscription is active or not
    /// this function is paired with the DB query which waits for this call to happen
    /// data_hash allows us to make sure the DB retrieves the same data that is updated on chain
    ///
    /// ```bash
    /// near call _pingbox_account_ update_subscription '{"owner": "dao.sputnik.testnet", "subscription_fee": "1234", "referral_reward": "1234"}' --accountId my_account.testnet
    /// ```
    pub fn update_subscription(&mut self, data_hash: Option<Base64VecU8>, active: Option<bool>) {
        // Only allow valid subscriptions
        let mut subscription = self
            .subscriptions
            .get(&env::predecessor_account_id())
            .expect("Must have valid subscription for this account");

        // update data
        if data_hash.is_some() {
            subscription.data_hash = Some(data_hash.unwrap().0);
        }
        if active.is_some() {
            subscription.active = active.unwrap();
        }

        // update data, only on the account you can sign for
        self.subscriptions
            .insert(&env::predecessor_account_id(), &subscription)
            .expect("Problem storing subscription updates");
    }

    /// Validate a referral code
    ///
    /// ```bash
    /// near view _pingbox_account_ get_referral_code '{"id": "some_account.testnet"}' --accountId my_account.testnet
    /// ```
    pub fn get_referral_code(&self, id: AccountId) -> String {
        let subscription = self
            .subscriptions
            .get(&id)
            .expect("Must have a subscription");

        subscription.referral_code
    }

    /// get array of active subscriptions ids
    ///
    /// ```bash
    /// near view _pingbox_account_ get_all_subscriptions
    /// ```
    pub fn get_all_subscriptions(&self) -> String {
        self.subscriptions.keys_as_vector().to_vec().join(",")
    }

    // get a single subscription item
    ///
    /// ```bash
    /// near view _pingbox_account_ get_subscription '{"id": "some_account.testnet"}'
    /// ```
    pub fn get_subscription(
        &self,
        id: AccountId,
    ) -> (
        bool,
        bool,
        BlockHeight,
        Balance,
        String,
        Base64VecU8,
        String,
        Option<Base64VecU8>,
    ) {
        let sub = self
            .subscriptions
            .get(&id)
            .expect("No subscription for this account id");

        (
            sub.active,
            sub.verified,
            sub.subscribe_block,
            sub.balance,
            sub.referral_code,
            Base64VecU8::from(sub.account_hash),
            self.sub_account(id),
            Some(Base64VecU8::from(sub.data_hash.unwrap_or_default())),
        )
    }

    /// get stats about Manager
    ///
    /// ```bash
    /// near view _pingbox_account_ get_stats
    /// ```
    pub fn get_stats(&self) -> (String, u128, u128, u128, u64, u64) {
        (
            env!("CARGO_PKG_VERSION").to_string(),
            self.subscription_fee,
            self.validation_reward,
            self.referral_reward,
            self.subscriptions.len(),
            self.referrals.len(),
        )
    }

    /// change the contract basic parameters, in case of needing to upgrade
    /// or change to different account IDs later.
    ///
    /// ```bash
    /// near call _pingbox_account_ update_settings '{"owner": "dao.sputnik.testnet", "settings_contract": "settings.in.testnet", "subscription_fee": "1234", "referral_reward": "1234"}' --accountId my_account.testnet
    /// ```
    pub fn update_settings(
        &mut self,
        owner: Option<ValidAccountId>,
        settings_contract: Option<ValidAccountId>,
        subscription_fee: Option<U128>,
        referral_reward: Option<U128>,
    ) {
        assert_eq!(
            self.owner.clone(),
            env::predecessor_account_id(),
            "Callee must be owner"
        );

        // Update each individual setting
        if owner.is_some() {
            self.owner = owner.unwrap().to_string();
        }
        if settings_contract.is_some() {
            self.settings_contract = settings_contract.unwrap().to_string();
        }

        // updated required subscription payment
        if subscription_fee.is_some() && subscription_fee.unwrap().0 > 0 {
            self.subscription_fee = subscription_fee.unwrap().0;
        }

        // update referral rewards
        if referral_reward.is_some() && referral_reward.unwrap().0 > 0 {
            self.referral_reward = referral_reward.unwrap().0;
        }
    }

    /// Allows admin to transfer value back to admin account
    /// Allows admin to transfer value out to subscription account if need be
    ///
    /// ```bash
    /// near call _pingbox_account_ admin_transfer_balance '{"id": "dao.sputnik.testnet", "amount": "1234"}' --accountId _pingbox_account_
    /// ```
    pub fn admin_transfer_balance(&self, id: AccountId, amount: U128) -> Promise {
        self.only_owner();
        // Require amount is less than contract balance
        assert!(
            amount.0 < env::account_balance(),
            "Not enough balance to transfer"
        );

        Promise::new(id).transfer(amount.0)
    }

    /// Admin: Pause subscription
    ///
    /// ```bash
    /// near call _pingbox_account_ admin_change_subscription '{"id": "subscription_user.testnet", "active": false, "remove": false}' --accountId _pingbox_account_
    /// ```
    pub fn admin_change_subscription(
        &mut self,
        id: AccountId,
        active: Option<bool>,
        remove: Option<bool>,
    ) {
        self.only_owner();
        assert_ne!(
            &id,
            &env::predecessor_account_id(),
            "Cannot change admin subscription"
        );

        let mut sub = self.subscriptions.get(&id).expect("No subscription found");

        if active.is_some() {
            sub.active = active.unwrap() || false;
            self.subscriptions.insert(&id, &sub);
        }

        if remove.is_some() && remove.unwrap() == true {
            self.subscriptions.remove(&id);
        }
    }

    /// Helper: Returns hashified string used in all other variables
    /// TYPE: VIEW ONLY
    ///
    /// ```bash
    /// near view settings.testnet sub_account '{"id": "account_id.in.testnet"}'
    /// ```
    pub fn sub_account(&self, id: AccountId) -> String {
        // create a hash of the account
        let h = self.hash(id);

        // trim hash to shortened verison
        format!("{}.{}", &h[3..18], self.settings_contract)
    }

    /// Helper: Returns hashified string used in all other variables
    ///
    /// ```bash
    /// near view settings.testnet version
    /// ```
    pub fn hash(&self, s: String) -> String {
        // generate & return hash from string
        let i = env::sha256(s.as_bytes());
        let key: Vec<String> = i.iter().map(|b| format!("{:02x}", b)).collect();
        key.join("")
    }

    /// Returns semver of this contract.
    ///
    /// ```bash
    /// near view settings.testnet version
    /// ```
    pub fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }
}

#[cfg(not(target_arch = "wasm32"))]
#[cfg(test)]
mod tests {
    use super::*;
    use near_sdk::MockedBlockchain;
    use near_sdk::{testing_env, BlockHeight, PublicKey, VMContext};

    #[allow(dead_code)]
    const MAX_ALLOWANCE: u128 = u128::MAX;

    pub struct VMContextBuilder {
        context: VMContext,
    }

    impl VMContextBuilder {
        pub fn new() -> Self {
            Self {
                context: VMContext {
                    current_account_id: "".to_string(),
                    signer_account_id: "".to_string(),
                    signer_account_pk: vec![0, 1, 2],
                    predecessor_account_id: "".to_string(),
                    input: vec![],
                    block_index: 0,
                    epoch_height: 0,
                    block_timestamp: 0,
                    account_balance: 0,
                    account_locked_balance: 0,
                    storage_usage: 10u64.pow(6),
                    attached_deposit: 0,
                    prepaid_gas: 10u64.pow(18),
                    random_seed: vec![0, 1, 2],
                    is_view: false,
                    output_data_receivers: vec![],
                },
            }
        }

        #[allow(dead_code)]
        pub fn current_account_id(mut self, account_id: AccountId) -> Self {
            self.context.current_account_id = account_id;
            self
        }

        #[allow(dead_code)]
        pub fn signer_account_id(mut self, account_id: AccountId) -> Self {
            self.context.signer_account_id = account_id;
            self
        }

        #[allow(dead_code)]
        pub fn predecessor_account_id(mut self, account_id: AccountId) -> Self {
            self.context.predecessor_account_id = account_id;
            self
        }

        #[allow(dead_code)]
        pub fn block_index(mut self, block_index: BlockHeight) -> Self {
            self.context.block_index = block_index;
            self
        }

        #[allow(dead_code)]
        pub fn attached_deposit(mut self, amount: Balance) -> Self {
            self.context.attached_deposit = amount;
            self
        }

        #[allow(dead_code)]
        pub fn account_balance(mut self, amount: Balance) -> Self {
            self.context.account_balance = amount;
            self
        }

        #[allow(dead_code)]
        pub fn account_locked_balance(mut self, amount: Balance) -> Self {
            self.context.account_locked_balance = amount;
            self
        }

        #[allow(dead_code)]
        pub fn signer_account_pk(mut self, pk: PublicKey) -> Self {
            self.context.signer_account_pk = pk;
            self
        }

        #[allow(dead_code)]
        pub fn finish(self) -> VMContext {
            self.context
        }
    }

    #[allow(dead_code)]
    fn alerts_near() -> String {
        "alerts_near".to_string()
    }

    #[allow(dead_code)]
    fn user_near() -> String {
        "user_near".to_string()
    }

    #[test]
    fn test_create_subscription() {
        testing_env!(VMContextBuilder::new()
            .current_account_id(alerts_near())
            .finish());
        let mut contract = Contract::default();
        let payment = ACCESS_KEY_ALLOWANCE * 100;
        testing_env!(VMContextBuilder::new()
            .current_account_id(user_near())
            .attached_deposit(payment)
            .finish());
        contract.add_subscription(Base64VecU8::from(vec![0, 1]), None);
        // let new_sub = contract.get_subscription(user_near());
        // assert_eq!(new_sub.account, user_near());
    }
}
