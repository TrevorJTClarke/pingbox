Pingbox Subscription Contract:
- Deployed to pingbox.near
- add_subscription
- validate_subscription
- update_subscription
- get_referral_code
- admin_update_fees
- admin_transfer_balance
- admin_pause_subscription
- admin_transfer_ownership
- get_all_subscriptions
- get_subscription
- get_stats

## Contracts:
pingbox.testnet

## Commands

```
# user fns
near call __ add_subscription '{"hash": [0,1,2]}' --amount 12.5 --accountId t.testnet
near call __ validate_subscription '{"hash": [0,1,2]}' --accountId t.testnet
near call __ update_subscription '{"data_hash": [0,1,2,3,4], "active": true}' --accountId t.testnet
near view __ get_referral_code --accountId t.testnet

# admin fns
near call __ admin_update_fees '{"subscription_amount": 11, "subscription_verify_fee": 1, "referral_reward": 1}' --accountId t.testnet
near call __ admin_transfer_balance '{"account_id": "t.testnet", "amount": 10}' --accountId t.testnet
near call __ admin_pause_subscription '{"account_id": "fake001.t.testnet", "active": false}' --accountId t.testnet
near call __ admin_remove_subscription '{"account_id": "fake001.t.testnet", "active": false}' --accountId t.testnet
near call __ admin_transfer_ownership '{"public_key": "Base58PublicKey"}' --accountId t.testnet
near view __ get_all_subscriptions --accountId t.testnet
near view __ get_subscription '{"account_id": "t.testnet"}' --accountId t.testnet
near view __ get_stats --accountId t.testnet
```

near call dev-1609449238857-6613339 add_subscription '{"hash": [0,1,2]}' --amount 12.5 --accountId t.testnet

near view dev-1609449238857-6613339 get_stats --accountId t.testnet
near view dev-1609449238857-6613339 get_all_subscriptions --accountId t.testnet
near view dev-1609449238857-6613339 get_subscription '{"account_id": "t.testnet"}' --accountId t.testnet

near call pingbox.testnet admin_remove_subscription '{"account_id": "pineapple.testnet"}' --accountId pingbox.testnet

near view pingbox.testnet get_stats --accountId t.testnet
near view dev-1613538552297-8476231 get_stats --accountId t.testnet