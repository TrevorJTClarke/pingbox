#!/bin/bash
# Uncomment the desired network
export NEAR_ENV=testnet
# export NEAR_ENV=mainnet

export FACTORY=testnet
# export FACTORY=near
# export FACTORY=registrar

export MASTER_ACCOUNT=pa.testnet
export PINGBOX_ACCOUNT_ID=pingbox1.$MASTER_ACCOUNT
export SETTINGS_ACCOUNT_ID=settings7.$MASTER_ACCOUNT
export SUB_SETTINGS_ACCOUNT_ID=95bc2bd12473033.$SETTINGS_ACCOUNT_ID
export DAO_ACCOUNT_ID=dao.sputnikv2.testnet

export TITLE_ACCOUNT_ID=pb_user00000001.testnet
export TITLE_PK=ed25519:6Mzi9dRMSiPWYp7BgLJ2Lj6KPCcs48FwB93NgQ4LKSBo

# TODO: check that settings has NO access keys

# Deploy the user settings contract under settings main
# near call $SETTINGS_ACCOUNT_ID deploy '{}' --accountId $TITLE_ACCOUNT_ID --amount 1.337 --gas 300000000000000

# near call $SETTINGS_ACCOUNT_ID init '{}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

# near call $SETTINGS_ACCOUNT_ID delete_account '{}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

# near view $SETTINGS_ACCOUNT_ID hash '{"s":"dapp_a_1"}'

# near call $SETTINGS_ACCOUNT_ID update '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647", "v": "6273151f959616268004b58dbb21e5c851b7b8d04498b4aabee12291d22fc034"}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

#NOTE: is call because its cross-contract
# near call $SETTINGS_ACCOUNT_ID get '{"id": "'$TITLE_ACCOUNT_ID'", "k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647"}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

# near call $SETTINGS_ACCOUNT_ID remove '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647"}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

# PINGBOX REGISTRATIONs
#, "rfr": ""
#near call $PINGBOX_ACCOUNT_ID add_subscription '{"hash": "NTQsNTUsMTAwLDk3LDUzLDUxLDk4LDQ4LDUyLDk4LDEwMiw1Nyw1Myw1NSwxMDAsOTgsNDksNDksNTcsNTAsOTgsNTQsNTEsNTAsMTAyLDUwLDk5LDUxLDEwMSw1Nyw1MCw1NCwxMDAsOTcsMTAyLDQ4LDk5LDEwMiw5OCwxMDAsNTYsMTAxLDEwMCw5OCw1MCwxMDIsNTQsMTAxLDk4LDUxLDU3LDU2LDEwMSw1NSw1Nyw0OSw5Nyw0OSw1NCw1MSw5NywxMDIsNTEsNTM="}' --accountId $TITLE_ACCOUNT_ID --amount 3 --gas 300000000000000

# near call $PINGBOX_ACCOUNT_ID validate_subscription '{"hash": "NTQsNTUsMTAwLDk3LDUzLDUxLDk4LDQ4LDUyLDk4LDEwMiw1Nyw1Myw1NSwxMDAsOTgsNDksNDksNTcsNTAsOTgsNTQsNTEsNTAsMTAyLDUwLDk5LDUxLDEwMSw1Nyw1MCw1NCwxMDAsOTcsMTAyLDQ4LDk5LDEwMiw5OCwxMDAsNTYsMTAxLDEwMCw5OCw1MCwxMDIsNTQsMTAxLDk4LDUxLDU3LDU2LDEwMSw1NSw1Nyw0OSw5Nyw0OSw1NCw1MSw5NywxMDIsNTEsNTM="}' --accountId $TITLE_ACCOUNT_ID --gas 300000000000000

near call $PINGBOX_ACCOUNT_ID admin_transfer_balance '{"id": "in.testnet", "amount": "1000000000000000000000000"}' --accountId $PINGBOX_ACCOUNT_ID --gas 300000000000000

echo "Settings Flow checked"