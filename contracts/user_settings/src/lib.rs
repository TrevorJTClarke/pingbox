use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::collections::LookupMap;
use near_sdk::{env, near_bindgen, AccountId, BorshStorageKey, PanicOnDefault, Promise};

near_sdk::setup_alloc!();

#[derive(BorshStorageKey, BorshSerialize)]
pub enum StorageKeys {
    Settings,
}

// const SETTINGS_ACCOUNT_ID: &str = "settings6.in.testnet";
// const SETTINGS_ACCOUNT_ID: &str = "settings6.pa.testnet";
// const SETTINGS_ACCOUNT_ID: &str = "settings.pbx.testnet";
// const SETTINGS_ACCOUNT_ID: &str = env!("SETTINGS_CONTRACT_ID").to_string();

// Settings is a one-way hash map of how a user can store settings on chain
// without exposing too much knowledge about what the settings are for.
// Even if the user allows others to see the settings data
// and only reveals boolean or similar that is meaningless itself.
#[near_bindgen]
#[derive(BorshDeserialize, BorshSerialize, PanicOnDefault)]
pub struct Settings {
    // Owner
    owner: AccountId,

    // deployer of this contract, the root account
    deployer: AccountId,

    // TODO: change to store as vec u8?
    // Mapping: <ContractSettingId, SettingHash>
    // EX:: Hash{contract_account_id + setting_name}: Hash{boolean|enum}
    settings: LookupMap<String, String>,
}

#[near_bindgen]
impl Settings {
    /// Deploy from the root account
    /// ```bash
    /// near deploy --wasmFile res/user_settings_pingbox.wasm --initFunction new --initArgs '{}' --accountId YOUR_ACCOUNT.testnet
    /// ```
    #[init]
    pub fn new() -> Self {
        Settings {
            owner: env::signer_account_id(),
            deployer: env::predecessor_account_id(),
            settings: LookupMap::new(StorageKeys::Settings),
        }
    }

    /// Allows root account to remove
    ///
    /// ```bash
    /// near call _hash_.settings.testnet dead '{}' --accountId youraccount.testnet
    /// ```
    pub fn dead(&mut self) {
        // require only owner
        assert_eq!(
            self.deployer.clone(),
            env::predecessor_account_id(),
            "Only deployer can dead"
        );

        Promise::new(env::current_account_id())
            .delete_account(self.owner.clone());
    }

    /// adds/updates a single contract preference setting
    /// k: Hash{contract_account_id + setting_name}
    /// v: Hash{boolean|enum}
    ///
    /// ```bash
    /// near call _hash_.settings.testnet update '{"k": "a197f4d4e823e234...", "v": "6273151f95961626..."}' --accountId youraccount.testnet
    /// ```
    pub fn update(&mut self, k: String, v: String) {
        // require only owner
        assert_eq!(
            self.deployer.clone(),
            env::predecessor_account_id(),
            "Only owner can update"
        );

        self.settings.insert(&k, &v);
    }

    /// removes a single contract preference setting
    /// Likely not needed, unless user wants to revoke entirely
    /// k: Hash{contract_account_id + setting_name}
    ///
    /// ```bash
    /// near call _hash_.settings.testnet remove '{"k": "a197f4d4e823e234..."}' --accountId youraccount.testnet
    /// ```
    pub fn remove(&mut self, k: String) {
        // require only owner
        assert_eq!(
            self.deployer.clone(),
            env::predecessor_account_id(),
            "Only owner can remove"
        );

        self.settings.remove(&k);
    }

    /// Gets a single setting by key
    /// k: Hash{contract_account_id + setting_name}
    /// Returns:
    /// v: "Hash{boolean|enum}"
    ///
    /// ```bash
    /// near view _hash_.settings.testnet get '{"k": "a197f4d4e823e234..."}' --accountId youraccount.testnet
    /// ```
    pub fn get(&self, k: String) -> Option<String> {
        self.settings.get(&k)
    }

    /// Completely deletes this account,
    /// useful in the case a user wants to completely wipe their settings
    /// NOTE: This is dangerous, as all saved data will be lost.
    ///
    /// ```bash
    /// near call _hash_.settings.testnet nuke --accountId youraccount.testnet
    /// ```
    pub fn nuke(&mut self) -> Promise {
        // require only owner
        assert_eq!(
            self.owner.clone(),
            env::predecessor_account_id(),
            "Only owner can nuke"
        );

        Promise::new(env::current_account_id()).delete_account(self.owner.clone())
    }

    /// Returns semver of this contract.
    ///
    /// ```bash
    /// near view _hash_.settings.testnet version
    /// ```
    pub fn version(&self) -> String {
        env!("CARGO_PKG_VERSION").to_string()
    }
}

// TODO: TESTS
// - can own
// - can up
// - can rem
// - can get
// - can hash

// #[cfg(not(target_arch = "wasm32"))]
// #[cfg(test)]
// mod tests {
//     use super::*;
//     use near_sdk::MockedBlockchain;
//     use near_sdk::{testing_env, VMContext};

//     fn get_context(input: Vec<u8>, is_view: bool) -> VMContext {
//         VMContext {
//             current_account_id: "alice_near".to_string(),
//             signer_account_id: "bob_near".to_string(),
//             signer_account_pk: vec![0, 1, 2],
//             predecessor_account_id: "carol_near".to_string(),
//             input,
//             block_index: 0,
//             block_timestamp: 0,
//             account_balance: 0,
//             account_locked_balance: 0,
//             storage_usage: 0,
//             attached_deposit: 0,
//             prepaid_gas: 10u64.pow(18),
//             random_seed: vec![0, 1, 2],
//             is_view,
//             output_data_receivers: vec![],
//             epoch_height: 0,
//         }
//     }

//     // #[test]
//     // fn get_nonexistent_message() {
//     //     let context = get_context(vec![], true);
//     //     testing_env!(context);
//     //     let contract = Thing::default();
//     //     assert_eq!(None, contract.set_thing("francis.near".to_string()));
//     // }
// }
