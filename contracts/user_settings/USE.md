User Settings Contract:
- Deployed to USER_HASH.settings_pingbox.near
- Mapping: <ContractSettingId, SettingHash> :: {contract_account_id + setting_name}: {boolean|enum}
- Get setting by ContractSettingId
- Update setting
- Remove setting

## Contracts:
bingpox.testnet
tings_bingpox.testnet
hu0001.tings_bingpox.testnet
hu0001_tings_bingpox.testnet

## Commands

```
# user fns
near deploy hu0001_tings_bingpox.testnet --wasmFile ./contracts/dist/user_settings_pingbox.wasm --masterAccount t.testnet
near call dev-1610236423337-9565516 update '{"k":"dapp_a_1","v":"true"}' --accountId dev-1610236423337-9565516
near view dev-1610236423337-9565516 get '{"k":"dapp_a_1"}' --accountId t.testnet
near call dev-1610236423337-9565516 remove '{"k":"dapp_a_1"}' --accountId t.testnet
```


near call dev-1610236423337-9565516 update '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647","v":"6273151f959616268004b58dbb21e5c851b7b8d04498b4aabee12291d22fc034"}' --accountId dev-1610236423337-9565516

near view dev-1610236423337-9565516 get '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647"}' --accountId dev-1610236423337-9565516

near call dev-1610236423337-9565516 remove '{"k":"a197f4d4e823e23480df4e0bde0e19256e35b3236163189364dfefc808602647"}' --accountId dev-1610236423337-9565516